import {
    mindrawerWidth,
    transition,
    container
  } from "assets/jss/material-dashboard-react.js";
  import { whiteColor } from "../../material-dashboard-react";
  
  
  const appStyle = theme => ({
    wrapper: {
      position: "relative",
      top: "0",
      height: "100vh"
    },
    mainPanel: {
      [theme.breakpoints.up("md")]: {
        width: `calc(100% - ${mindrawerWidth}px)`
      },
      overflow: "auto",
      position: "relative",
      float: "right",
      ...transition,
      maxHeight: "100%",
      width: "100%",
      overflowScrolling: "touch",
      backgroundColor: whiteColor
    },
    content: {
      marginTop: "70px",
      padding: "50px 35px",
      minHeight: "calc(100vh - 123px)"
    },
    container,
    map: {
      marginTop: "70px"
    }
  });
    
  export default appStyle
  