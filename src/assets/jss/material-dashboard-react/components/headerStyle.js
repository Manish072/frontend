import {
  container,
  defaultFont,
  primaryColor,
  defaultBoxShadow,
  infoColor,
  successColor,
  warningColor,
  dangerColor,
  whiteColor,
  grayColor
} from "assets/jss/material-dashboard-react.js";

const headerStyle = () => ({
  appBar: {
    backgroundColor: "transparent",
    boxShadow: "none",
    borderBottom: "0",
    marginBottom: "0",
    position: "absolute",
    width: "100%",
    paddingTop: "10px",
    zIndex: "1029",
    color: grayColor[7],
    border: "0",
    borderRadius: "3px",
    padding: "10px 0",
    transition: "all 150ms ease 0s",
    minHeight: "50px",
    display: "block"
  },
  container: {
    ...container,
    minHeight: "50px",
    outline: "none"
  },
  isoSwit:{
    paddingTop:"10px",
    marginLeft: "10px"
  },
  menuItem: {
    borderRadius: 80 / 2,
    width: "280px",
    height: "75px",
    flex:"2",
    outline: "none",
    // border: `1px  ${theme.palette.grey[400]}`,
    // backgroundColor: theme.palette.grey[200],
    opacity: 100,
    // transition: theme.transitions.create(["background-color", "border"])
  },
  connectionIconGrey:{
    position:"relative",
    width: "18px",
    height: "18px",
    border: "solid 0px #d8d8d8",
    borderRadius:"250px",
    marginRight: "10px",
    marginBottom: "-3px",
    backgroundColor: "#d8d8d8",
    flexDirection: "row",
    display:"inline-block",
    outline: "none"
  },
  connectionIconGreen:{
    position:"relative",
    width: "18px",
    height: "18px",
    border: "solid 0px #34af37",
    borderRadius:"250px",
    marginRight: "10px",
    marginBottom: "-3px",
    backgroundColor: "#34af37",
    flexDirection: "row",
    display:"inline-block",
    outline: "none"
  },
  databaseText:{
    width: "300px",
    height: "43px",
    flexDirection: "row",
    display:"inline-block",
    padding:"5px 5px 5px 0px",
    outline: "none"
  }
  ,
  routeName:{
    margin: "10px",
    fontFamily:"Roboto",
    marginLeft: "36px",
    fontWeight: "500",
    fontSize: "18px",
    padding: "6px -1px",
    color: "#004162"
  },
  dropBut:{
    boxShadow: "none",
  //   width: "217px",
  // height: "18px",
  // fontFamily: "Effra",
  flexDirection: "row",
  fontFamily: [
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    'Roboto',
    '"Helvetica Neue"',
    'Arial',
    'sans-serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
  ].join(','),
  fontSize: "16px",
  fontWeight: "600",
  fontStretch: "normal",
  fontStyle: "normal",
  lineHeight: "1.39",
  letterSpacing: "normal",
    textTransform:"none",
    padding:"10px",
    outline: "none"
  },
  dropdow:{
    marginBottom:"-10px",
    marginLeft:"-25px",
    textTransform: "none",
    outline: "none",
    boxShadow:"none"
    // flex:"1",
    // flexDirection: "row"
  },
  welcome:{
    marginLeft: "200px"
  },
  flex: {
    flex: "1",
    flexDirection: "row",
    marginLeft: "250px",
    fontSize: "16px",
    color: "#004162"
  },
  flex1:{
    flex: "1",
    flexDirection: "row",
    marginLeft: "550px",
    fontSize: "16px",
    color: "#004162"
  },
  flex2:{
    flex: "1",
    flexDirection: "row",
    marginLeft: "500px",
    fontSize: "16px",
    color: "#004162"
  },
  Divide: {
  marginLeft: "1px"
  },
  title: {
    // ...defaultFont,
    fontFamily:"Roboto",
    letterSpacing: "unset",
    lineHeight: "30px",
    fontSize: "20px",
    fontWeight: "500",
    borderRadius: "3px",
    textTransform: "none",
    color: "inherit",
    margin: "0",
    "&:hover,&:focus": {
      background: "transparent"
    }
  },
  appResponsive: {
    top: "8px"
  },
  primary: {
    backgroundColor: primaryColor[0],
    color: whiteColor,
    ...defaultBoxShadow
  },
  info: {
    backgroundColor: infoColor[0],
    color: whiteColor,
    ...defaultBoxShadow
  },
  success: {
    backgroundColor: successColor[0],
    color: whiteColor,
    ...defaultBoxShadow
  },
  warning: {
    backgroundColor: warningColor[0],
    color: whiteColor,
    ...defaultBoxShadow
  },
  danger: {
    backgroundColor: dangerColor[0],
    color: whiteColor,
    ...defaultBoxShadow
  },
   icons:{
     fontSize: "30px"

   },
   minflex: {
    flex: "1",
    flexDirection: "row",
    marginLeft: "300px",
    fontSize: "16px",
    color: "#004162"
  },
  minflex1: {
    flex: "1",
    flexDirection: "row",
    marginLeft: "600px",
    fontSize: "16px",
    color: "#004162"
  },
  minflex2: {
    flex: "1",
    flexDirection: "row",
    marginLeft: "550px",
    fontSize: "16px",
    color: "#004162"
  },

});

export default headerStyle;