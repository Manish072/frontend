import {
  defaultFont,
  container,
  primaryColor,
  grayColor,
  whiteColor,
} from "assets/jss/material-dashboard-react.js";

const footerStyle = {
  block: {
    color: "inherit",
    padding: "15px",
    textTransform: "uppercase",
    borderRadius: "3px",
    textDecoration: "none",
    position: "relative",
    display: "block",
    ...defaultFont,
    fontWeight: "500",
    fontSize: "12px"
  },
  left: {
    float: "left!important",
    display: "block"
  },
  right: {
    padding: "15px 0",
    margin: "0",
    fontSize: "14px",
    float: "right!important"
  },
  // footer: {
  //   bottom: "0",
  //   borderTop: "1px solid " + grayColor[11],
  //   padding: "0px 0",
  //   ...defaultFont
  // },
  footer:{
    width: "1366 px",
    height: "63 px",
    boxShadow:"3px 0 10px 0 rgba(0, 0, 0, 0.16)",
    border: "solid 1px #d3d3d3",
    backgroundColor: "#f8f8f8",
  },
  footertxt: {
    width: "134 px",
    height: "18 px",
    fontFamily: "Roboto",
    fontSize: "16px",
    fontWeight: "normal",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.39",
    letterSpacing: "normal",
    color: "#004162",
  },
  container,
  a: {
    color: primaryColor,
    textDecoration: "none",
    backgroundColor: "transparent"
  },
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0"
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0px",
    width: "auto"
  }
};
export default footerStyle;
