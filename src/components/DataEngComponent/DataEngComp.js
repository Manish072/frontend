import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import CustomizedDividers from '../ToggleComponent/ToggleGraph';
import CustomizedDividersSimulation from '../ToggleComponentSimulation/ToggleGraph';
import CustomizedDividersDeploy from '../Deployment/deployment'
// import { left } from "@popperjs/core";
import {useSelector,useDispatch} from 'react-redux';
import {simulation,deployment} from 'actions';
import { FcCheckmark } from 'react-icons/fc';
// import { border } from "@material-ui/system";
import './DataEngComp.css'
import CheckBoxSelection from "../Comparisions/CheckBoxSelection";


function TabPanel(props) {
  const { children, index, ...other } = props;
  const value=useSelector(state=>state.openTab);


  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,

  },
  customTabRoot: {
    color: "#004162",
    backgroundColor: "white",
    paddingLeft: theme.spacing(2),

  },
  customTabIndicator: {
    backgroundColor: "#004162",
    height:'3px',

  }
}));

export default function SimpleTabs(props) {
  const classes = useStyles();
 // const [value, setValue] = React.useState({value});
 const value=useSelector(state=>state);
 const dispatch=useDispatch();
  //  React.useEffect(() =>{
  //   setValue(value);
  //  },[value])

  const handleChange = (event, newValue) => {
 //   setValue(newValue);
  };
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"

          classes={{
            root: classes.customTabRoot,
            indicator: classes.customTabIndicator
          }}
        >
           {/* <Stepper></Stepper> */}
           {value !==0 ? <div><FcCheckmark></FcCheckmark><Tab label={<span className="tabLabel1">Model Evaluation</span>} {...a11yProps(0)} onClick={()=>dispatch(simulation())} /></div>:
          <div>1. <Tab label={<span className="tabLabel2">Model Evaluation</span>}{...a11yProps(0)} onClick={()=>dispatch(simulation())} /></div>}
          {/* <div>3. <Tab label={<span className="tabLabel2">Model Deployment</span>} {...a11yProps(2)} onClick={()=>dispatch(deployment())} /></div> */}
        </Tabs>
      </AppBar>
      {/* <TabPanel value={value} index={0}>
        <CustomizedDividers></CustomizedDividers>
      </TabPanel> */}
      <TabPanel value={value} index={0}>
        <CheckBoxSelection/>
      </TabPanel>
      <TabPanel value={value} index={1}>
       <CustomizedDividersDeploy/>
      </TabPanel>

    </div>
  );
}