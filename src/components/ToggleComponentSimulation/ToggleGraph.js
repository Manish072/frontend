import React, {Component} from 'react';
import {createMuiTheme, makeStyles, withStyles,ThemeProvider } from '@material-ui/core/styles';
import FormatAlignLeftIcon from '@material-ui/icons/FormatAlignLeft';
import FormatAlignCenterIcon from '@material-ui/icons/FormatAlignCenter';
import FormatAlignRightIcon from '@material-ui/icons/FormatAlignRight';
import FormatAlignJustifyIcon from '@material-ui/icons/FormatAlignJustify';
import FormatBoldIcon from '@material-ui/icons/FormatBold';
import FormatItalicIcon from '@material-ui/icons/FormatItalic';
import FormatUnderlinedIcon from '@material-ui/icons/FormatUnderlined';
import FormatColorFillIcon from '@material-ui/icons/FormatColorFill';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import { indigo, deepPurple, red, blue } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button';
import './ToggleGraph.css';
import Simulation from "../../components/Simulation/RadioSelection/RadioSelection"
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import Comparision from "../../components/ComparisonGraph/Comparision"
import CheckBoxSelection from '../../components/Comparisions/CheckBoxSelection';


const useStyles = theme => ({
  paper: {
    display: 'flex',
    border: `1px solid`,
    flexWrap: 'wrap',
    width: theme.spacing(30.5),
    color:'grey'
  },
button:
{
  marginLeft: theme.spacing(0.65),
  marginRight: theme.spacing(0.5),
  marginTop: theme.spacing(0.75),
  marginBottom: theme.spacing(0.75),
  boxShadow: '1000',
  textTransform: 'none',
  fontSize: 16,
  color: 'white',
  padding: '6px 12px',
  border: '1px solid',
  lineHeight: 1.95,
  backgroundColor: '#004162',
  borderColor: '#004162',
  fontFamily: [
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    'Roboto',
    '"Helvetica Neue"',
    'Arial',
    'sans-serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
  ].join(','),
  '&:hover': {
    backgroundColor: '#004162',
    borderColor: '#004162',
    boxShadow: 'none'
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: '#004162',
    borderColor: '#004162',
    
  },
  '&:focus': {
    backgroundColor: '#004162',
    borderColor: '#004162',
    color:'white'
  },
},
button1:
{
  marginLeft: theme.spacing(0.65),
  marginRight: theme.spacing(0.5),
  marginTop: theme.spacing(0.75),
  marginBottom: theme.spacing(0.75),
  boxShadow: '1000',
  textTransform: 'none',
  fontSize: 16,
  color: '#004162',
  padding: '6px 12px',
  border: '1px solid',
  lineHeight: 1.95,
  backgroundColor: '',
  borderColor: 'white',
  fontFamily: [
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    'Roboto',
    '"Helvetica Neue"',
    'Arial',
    'sans-serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
  ].join(','),
  '&:hover': {
    backgroundColor: 'white',
    borderColor: 'white',
    boxShadow: 'none'
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: 'white',
    borderColor: 'white',
    
  },
  '&:focus': {
    backgroundColor: '#004162',
    borderColor: '#004162',
    color:'white'
  },
},
});

// const StyledToggleButtonGroup = withStyles((theme) => ({
//   grouped: {
//     margin: theme.spacing(0.5),
//     border: '5',
//     '&:not(:first-child)': {
//       borderRadius: theme.shape.borderRadius,
//     },
//     '&:first-child': {
//       borderRadius: theme.shape.borderRadius,
//     },
//   },
// }))(ToggleButtonGroup);

// const theme = createMuiTheme({
//     palette: {
//       action:{
//         active: '#004162'
//       } 
//     }
//   });
 

export class CustomizedDividers extends Component{
  

  constructor(props) {
    super(props);
    this.state = { status: 1 }; // 0: no show, 1: show yes, 2: show no.
  }

  radioHandler = (status) => {
    this.setState({status});
  }

  // handleAlignment = () => {
  //   this.setState({
  //     alignment:'right'
  //   })
  // }
  
  render(){
  const {classes} = this.props;

  const { status, button} = this.state;

  return (
    <div>
    <div>
      {/* <Paper elevation={0} className={classes.paper}> */}
      {/* <ThemeProvider theme={theme}>
        <StyledToggleButtonGroup
          size="small"
          value={'left'}
          exclusive
          onChange={this.handleAlignment}
          aria-label="text alignment"
        >
          <ToggleButton classes={
              {root: classes.root}
          }
          value="left" aria-label="left aligned" 
          checked={status === 1}
          onClick={(e) => this.radioHandler(1)}
          >
            Database Configuration
          </ToggleButton>
          <ToggleButton classes={
              {root: classes.root}}
              value="right" aria-label="right aligned"
              checked={status === 2}
              onClick={(e) => this.radioHandler(2)}>
            Upload CSV
          </ToggleButton>
        </StyledToggleButtonGroup>
        </ThemeProvider> */}
        <MuiThemeProvider>
        <>
        <Paper elevation={0} className={classes.paper}>
       {(status ===1)?<Button
          className={classes.button}
          checked={status === 1}
          onClick={(e) => this.radioHandler(1)}
          >
          Simulation
        </Button>:<Button
          className={classes.button1}
          checked={status === 1}
          onClick={(e) => this.radioHandler(1)}
          >
          Simulation
        </Button>}
        {(status ===2)?<Button
          className={classes.button}
          checked={status === 2}
          onClick={(e) => this.radioHandler(2)}
          >
          Comparision
        </Button>:<Button
          className={classes.button1}
          checked={status === 2}
          onClick={(e) => this.radioHandler(2)}
          >
          Comparision
        </Button>}
        </Paper>
        </>
        </MuiThemeProvider>
      {/* </Paper> */}
    </div>
    {(status === 1)?<Simulation/>:null}
    {(status === 2)?<CheckBoxSelection/>:null}
    </div>
  );
  }
}

export default withStyles(useStyles)(CustomizedDividers);

