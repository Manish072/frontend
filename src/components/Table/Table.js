import React, { Component } from "react";
import Dialog from '@material-ui/core/Dialog';
import ReactFileReader from 'react-file-reader';
import { CsvToHtmlTable } from 'react-csv-to-table';
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import theme from './Theme';
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import { Button } from 'react-bootstrap';
import Button1 from '@material-ui/core/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
// import './style.css';
import bsCustomFileInput from 'bs-custom-file-input';
import './Table.css'
// core components
//import styles from "assets/jss/material-dashboard-react/components/tableStyle.js";
import { render } from "react-dom";
import { JsonToTable } from "react-json-to-table";
import axios from "axios";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { isExternalModuleNameRelative } from "typescript";

const useStyles = theme => ({
  table: {
    minWidth: 650,
  },
  // formControl: {
  //   margin: theme.spacing(1),
  //   minWidth: 120,
  // },
  button: {
    boxShadow: '1000',
    textTransform: 'none',
    fontSize: 16,
    color: 'white',
    padding: '6px 12px',
    border: '1px solid',
    lineHeight: 1.95,
    backgroundColor: '#c73232',
    borderColor: '#c73232',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#c73232',
      borderColor: '#c73232',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#c73232',
      borderColor: '#c73232',
      outline: 'none'
    },
    '&:focus': {
      boxShadow: 'none',
      outline: 'none'
    },
  },
  groupButton: {
    marginTop: theme.spacing(7),
    paddingLeft: theme.spacing(115)
  },
  dialog: {
    width: "450px",
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    fontSize: "20px",
    fontWeight: "400",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.25",
    letterSpacing: "normal",
    textAlign: "left",
    color: "#2e2e2e",
    textTransform: "none"
  },
  confmColo: {
    marginLeft: "-7px",
    color: "#032e58",
    fontFamily: "Effra",
    fontSize: "20px",
    fontWeight: 500,
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.4,
    letterSpacing: "normal",
    textAlign: "left",
    textTransform: "none"
  },
  notConf: {
    textTransform: "none",
    paddingLeft: "30px",
    color: "#032e58",
  },
  confir: {
    backgroundColor: "#c73232",
    color: "white",
    "&:hover": {
      backgroundColor: "#c73232"
    },
    width: "100px",
    textTransform: "none"
  },
  dialogAct: {
    padding: "17px",
    marginLeft: "-8px"
  },
  reset: {
    marginLeft: theme.spacing(2),
    paddingRight: theme.spacing(5),
    //boxShadow: '1000',
    textTransform: 'none',
    fontSize: 16,
    color: '#004162',
    padding: '6px 12px',
    //border: '1px solid',
    lineHeight: 1.95,
    backgroundColor: 'white',
    boxShadow: 'none',
    //borderColor: 'black',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: 'white',
      borderColor: 'black',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: 'white',
      borderColor: 'black',
      outline: 'none'
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      boxShadow: 'none',
      outline: 'none'
    },
  },

});

const styles = {
  row: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: '16px'
  },
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
}

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
    direction: "rtl"
  }
}))(MuiDialogActions);


// const sampleData = `No Data Uploaded..`;

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      csvData: null,
      csvHeaders: [],
      dict: [],
      col1: '',
      col2: '',
      col3: '',
      col4: '',
      col5: '',
      col6: '',
      col7: '',
      col8: '',
      col9: '',
      dropDownList1: [],
      dropDownList2: [],
      dropDownList3: [],
      dropDownList4: [],
      dropDownList5: [],
      dropDownList6: [],
      dropDownList7: [],
      dropDownList8: [],
      dropDownList9: [],
      csvFileLoaded: false,
      showDataTable: false,
      sampleData : "No data found. Upload the CSV file.",
      uploadAlert: false,
      open: false,
      error1: false,
      error2: false,
      error3: false,
      error4: false,
      error5: false,
      error6: false,
      error7: false,
      error8: false,
      error9: false,
      //allFieldsSelected: true

    };
  }


  componentDidMount() {
    bsCustomFileInput.init()
  }

  handleChange1 = (event) => {

    const { dropDownList1, dropDownList2, dropDownList3, dropDownList4, dropDownList5, dropDownList6, dropDownList7, dropDownList8, dropDownList9 } = this.state;
    for (const [key, value] of this.state.dict.entries()) {
      if (value[0]["key"] === event.target.value) {
        if ((isNaN(Date.parse(value[0]["value"]))) || (!value[0]["value"].match(/\D/g))) {
          this.setState({
            error1: true
          });
          return;
        }
      }
    }

    this.setState({
      col1: event.target.value,
      error1: false
    });

    var i = 0;
    while (dropDownList1.length !== 1) {
      if (dropDownList1[i] === event.target.value) {
        ++i;
      } else {
        dropDownList1.splice(i, 1);
      }
    }
    i = 0;
    while (i < dropDownList2.length) {
      if (dropDownList2[i] === event.target.value) {
        dropDownList2.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }

    i = 0;
    while (i < dropDownList3.length) {
      if (dropDownList3[i] === event.target.value) {
        dropDownList3.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList4.length) {
      if (dropDownList4[i] === event.target.value) {
        dropDownList4.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList5.length) {
      if (dropDownList5[i] === event.target.value) {
        dropDownList5.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList6.length) {
      if (dropDownList6[i] === event.target.value) {
        dropDownList6.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList7.length) {
      if (dropDownList7[i] === event.target.value) {
        dropDownList7.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList8.length) {
      if (dropDownList8[i] === event.target.value) {
        dropDownList8.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList9.length) {
      if (dropDownList9[i] === event.target.value) {
        dropDownList9.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
  };
  handleChange2 = (event) => {

    const { dropDownList1, dropDownList2, dropDownList3, dropDownList4, dropDownList5, dropDownList6, dropDownList7, dropDownList8, dropDownList9 } = this.state;
    for (const [key, value] of this.state.dict.entries()) {
      if (value[0]["key"] === event.target.value) {
        if (!value[0]["value"].match(/^[A-Z]+$/i)) {
          this.setState({
            error2: true
          });
          return;
        }
      }
    }
    this.setState({
      col2: event.target.value,
      error2: false
    });

    var i = 0;
    while (dropDownList2.length !== 1) {
      if (dropDownList2[i] === event.target.value) {
        ++i;
      } else {
        dropDownList2.splice(i, 1);
      }
    }
    i = 0;
    while (i < dropDownList1.length) {
      if (dropDownList1[i] === event.target.value) {
        dropDownList1.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList3.length) {
      if (dropDownList3[i] === event.target.value) {
        dropDownList3.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList4.length) {
      if (dropDownList4[i] === event.target.value) {
        dropDownList4.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList5.length) {
      if (dropDownList5[i] === event.target.value) {
        dropDownList5.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList6.length) {
      if (dropDownList6[i] === event.target.value) {
        dropDownList6.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList7.length) {
      if (dropDownList7[i] === event.target.value) {
        dropDownList7.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList8.length) {
      if (dropDownList8[i] === event.target.value) {
        dropDownList8.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList9.length) {
      if (dropDownList9[i] === event.target.value) {
        dropDownList9.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
  };
  handleChange3 = (event) => {
    const { dropDownList1, dropDownList2, dropDownList3, dropDownList4, dropDownList5, dropDownList6, dropDownList7, dropDownList8, dropDownList9 } = this.state;
    for (const [key, value] of this.state.dict.entries()) {
      if (value[0]["key"] === event.target.value) {
        if (!value[0]["value"].match(/^[A-Z]+$/i)) {
          this.setState({
            error3: true
          });
          return;
        }
      }
    }
    this.setState({
      col3: event.target.value,
      error3: false
    });

    var i = 0;
    while (dropDownList3.length !== 1) {
      if (dropDownList3[i] === event.target.value) {
        ++i;
      } else {
        dropDownList3.splice(i, 1);
      }
    }
    i = 0;
    while (i < dropDownList1.length) {
      if (dropDownList1[i] === event.target.value) {
        dropDownList1.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList2.length) {
      if (dropDownList2[i] === event.target.value) {
        dropDownList2.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList4.length) {
      if (dropDownList4[i] === event.target.value) {
        dropDownList4.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList5.length) {
      if (dropDownList5[i] === event.target.value) {
        dropDownList5.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList6.length) {
      if (dropDownList6[i] === event.target.value) {
        dropDownList6.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList7.length) {
      if (dropDownList7[i] === event.target.value) {
        dropDownList7.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList8.length) {
      if (dropDownList8[i] === event.target.value) {
        dropDownList8.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList9.length) {
      if (dropDownList9[i] === event.target.value) {
        dropDownList9.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
  };
  handleChange4 = (event) => {
    const { dropDownList1, dropDownList2, dropDownList3, dropDownList4, dropDownList5, dropDownList6, dropDownList7, dropDownList8, dropDownList9 } = this.state;
    for (const [key, value] of this.state.dict.entries()) {
      if (value[0]["key"] === event.target.value) {
        if (!value[0]["value"].match(/^[A-Z]+$/i)) {
          this.setState({
            error4: true
          });
          return;
        }
      }
    }
    this.setState({
      col4: event.target.value,
      error4: false
    });

    var i = 0;
    while (dropDownList4.length !== 1) {
      if (dropDownList4[i] === event.target.value) {
        ++i;
      } else {
        dropDownList4.splice(i, 1);
      }
    }
    i = 0;
    while (i < dropDownList1.length) {
      if (dropDownList1[i] === event.target.value) {
        dropDownList1.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList2.length) {
      if (dropDownList2[i] === event.target.value) {
        dropDownList2.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList3.length) {
      if (dropDownList3[i] === event.target.value) {
        dropDownList3.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList5.length) {
      if (dropDownList5[i] === event.target.value) {
        dropDownList5.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList6.length) {
      if (dropDownList6[i] === event.target.value) {
        dropDownList6.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList7.length) {
      if (dropDownList7[i] === event.target.value) {
        dropDownList7.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList8.length) {
      if (dropDownList8[i] === event.target.value) {
        dropDownList8.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList9.length) {
      if (dropDownList9[i] === event.target.value) {
        dropDownList9.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
  };
  handleChange5 = (event) => {
    const { dropDownList1, dropDownList2, dropDownList3, dropDownList4, dropDownList5, dropDownList6, dropDownList7, dropDownList8, dropDownList9 } = this.state;
    for (const [key, value] of this.state.dict.entries()) {
      if (value[0]["key"] === event.target.value) {
        if (isNaN(value[0]["value"])) {
          this.setState({
            error5: true
          });
          return;
        }
      }
    }
    this.setState({
      col5: event.target.value,
      error5: false
    });

    var i = 0;
    while (dropDownList5.length !== 1) {
      if (dropDownList5[i] === event.target.value) {
        ++i;
      } else {
        dropDownList5.splice(i, 1);
      }
    }
    i = 0;
    while (i < dropDownList1.length) {
      if (dropDownList1[i] === event.target.value) {
        dropDownList1.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList2.length) {
      if (dropDownList2[i] === event.target.value) {
        dropDownList2.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList3.length) {
      if (dropDownList3[i] === event.target.value) {
        dropDownList3.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList4.length) {
      if (dropDownList4[i] === event.target.value) {
        dropDownList4.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }

    i = 0;
    while (i < dropDownList6.length) {
      if (dropDownList6[i] === event.target.value) {
        dropDownList6.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList7.length) {
      if (dropDownList7[i] === event.target.value) {
        dropDownList7.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList8.length) {
      if (dropDownList8[i] === event.target.value) {
        dropDownList8.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList9.length) {
      if (dropDownList9[i] === event.target.value) {
        dropDownList9.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
  };
  handleChange6 = (event) => {
    const { dropDownList1, dropDownList2, dropDownList3, dropDownList4, dropDownList5, dropDownList6, dropDownList7, dropDownList8, dropDownList9 } = this.state;
    for (const [key, value] of this.state.dict.entries()) {
      if (value[0]["key"] === event.target.value) {
        if (isNaN(value[0]["value"])) {
          this.setState({
            error6: true
          });
          return;
        }
      }
    }
    this.setState({
      col6: event.target.value,
      error6: false
    });

    var i = 0;
    while (dropDownList6.length !== 1) {
      if (dropDownList6[i] === event.target.value) {
        ++i;
      } else {
        dropDownList6.splice(i, 1);
      }
    }
    i = 0;
    while (i < dropDownList1.length) {
      if (dropDownList1[i] === event.target.value) {
        dropDownList1.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList2.length) {
      if (dropDownList2[i] === event.target.value) {
        dropDownList2.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList3.length) {
      if (dropDownList3[i] === event.target.value) {
        dropDownList3.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList4.length) {
      if (dropDownList4[i] === event.target.value) {
        dropDownList4.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList5.length) {
      if (dropDownList5[i] === event.target.value) {
        dropDownList5.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }

    i = 0;
    while (i < dropDownList7.length) {
      if (dropDownList7[i] === event.target.value) {
        dropDownList7.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList8.length) {
      if (dropDownList8[i] === event.target.value) {
        dropDownList8.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList9.length) {
      if (dropDownList9[i] === event.target.value) {
        dropDownList9.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
  };
  handleChange7 = (event) => {
    const { dropDownList1, dropDownList2, dropDownList3, dropDownList4, dropDownList5, dropDownList6, dropDownList7, dropDownList8, dropDownList9 } = this.state;
    for (const [key, value] of this.state.dict.entries()) {
      if (value[0]["key"] === event.target.value) {
        if (isNaN(value[0]["value"])) {
          this.setState({
            error7: true
          });
          return;
        }
      }
    }
    this.setState({
      col7: event.target.value,
      error7: false
    });

    var i = 0;
    while (dropDownList7.length !== 1) {
      if (dropDownList7[i] === event.target.value) {
        ++i;
      } else {
        dropDownList7.splice(i, 1);
      }
    }
    i = 0;
    while (i < dropDownList1.length) {
      if (dropDownList1[i] === event.target.value) {
        dropDownList1.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList2.length) {
      if (dropDownList2[i] === event.target.value) {
        dropDownList2.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList3.length) {
      if (dropDownList3[i] === event.target.value) {
        dropDownList3.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList4.length) {
      if (dropDownList4[i] === event.target.value) {
        dropDownList4.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList5.length) {
      if (dropDownList5[i] === event.target.value) {
        dropDownList5.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList6.length) {
      if (dropDownList6[i] === event.target.value) {
        dropDownList6.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }

    i = 0;
    while (i < dropDownList8.length) {
      if (dropDownList8[i] === event.target.value) {
        dropDownList8.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList9.length) {
      if (dropDownList9[i] === event.target.value) {
        dropDownList9.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
  };
  handleChange8 = (event) => {
    const { dropDownList1, dropDownList2, dropDownList3, dropDownList4, dropDownList5, dropDownList6, dropDownList7, dropDownList8, dropDownList9 } = this.state;
    for (const [key, value] of this.state.dict.entries()) {
      if (value[0]["key"] === event.target.value) {
        if (isNaN(value[0]["value"])) {
          this.setState({
            error8: true
          });
          return;
        }
      }
    }
    this.setState({
      col8: event.target.value,
      error8: false
    });

    var i = 0;
    while (dropDownList8.length !== 1) {
      if (dropDownList8[i] === event.target.value) {
        ++i;
      } else {
        dropDownList8.splice(i, 1);
      }
    }
    i = 0;
    while (i < dropDownList1.length) {
      if (dropDownList1[i] === event.target.value) {
        dropDownList1.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList2.length) {
      if (dropDownList2[i] === event.target.value) {
        dropDownList2.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList3.length) {
      if (dropDownList3[i] === event.target.value) {
        dropDownList3.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList4.length) {
      if (dropDownList4[i] === event.target.value) {
        dropDownList4.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList5.length) {
      if (dropDownList5[i] === event.target.value) {
        dropDownList5.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList6.length) {
      if (dropDownList6[i] === event.target.value) {
        dropDownList6.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList7.length) {
      if (dropDownList7[i] === event.target.value) {
        dropDownList7.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }

    i = 0;
    while (i < dropDownList9.length) {
      if (dropDownList9[i] === event.target.value) {
        dropDownList9.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
  };
  handleChange9 = (event) => {
    const { dropDownList1, dropDownList2, dropDownList3, dropDownList4, dropDownList5, dropDownList6, dropDownList7, dropDownList8, dropDownList9 } = this.state;
    for (const [key, value] of this.state.dict.entries()) {
      if (value[0]["key"] === event.target.value) {
        if (isNaN(value[0]["value"])) {
          this.setState({
            error9: true
          });
          return;
        }
      }
    }
    this.setState({
      col9: event.target.value,
      error9: false
    });

    var i = 0;
    while (dropDownList9.length !== 1) {
      if (dropDownList9[i] === event.target.value) {
        ++i;
      } else {
        dropDownList9.splice(i, 1);
      }
    }
    i = 0;
    while (i < dropDownList1.length) {
      if (dropDownList1[i] === event.target.value) {
        dropDownList1.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList2.length) {
      if (dropDownList2[i] === event.target.value) {
        dropDownList2.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList3.length) {
      if (dropDownList3[i] === event.target.value) {
        dropDownList3.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList4.length) {
      if (dropDownList4[i] === event.target.value) {
        dropDownList4.splice(i, 1);
        break;
      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList5.length) {
      if (dropDownList5[i] === event.target.value) {
        dropDownList5.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList6.length) {
      if (dropDownList6[i] === event.target.value) {
        dropDownList6.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList7.length) {
      if (dropDownList7[i] === event.target.value) {
        dropDownList7.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }
    i = 0;
    while (i < dropDownList8.length) {
      if (dropDownList8[i] === event.target.value) {
        dropDownList8.splice(i, 1);
        break;

      } else {
        ++i;
      }
    }

  };



  onFileChangeHandler = (e) => {
    let reader;
    console.log(e);
    const handleFileRead = () => {
      const content = reader.result;
      var headers = [];
      headers = content.split("\n")[0].split(",");
      var row2 = content.split("\n")[1].split(",");
      console.log(row2);
      var i = 0;
      while (i < row2.length) {
        console.log('while loop....');
        var arr = []; // create an empty array

        arr.push({
          key: headers[i],
          value: row2[i]
        });
        this.setState(prevState => ({
          dict: [...prevState.dict, arr]
        }))
        i++;
      }
      console.log(this.state.dict);
      this.setState({
        csvHeaders: headers.slice(),
        dropDownList1: headers.slice(),
        dropDownList2: headers.slice(),
        dropDownList3: headers.slice(),
        dropDownList4: headers.slice(),
        dropDownList5: headers.slice(),
        dropDownList6: headers.slice(),
        dropDownList7: headers.slice(),
        dropDownList8: headers.slice(),
        dropDownList9: headers.slice()
      })

    };
    reader = new FileReader();
    reader.onloadend = handleFileRead;
    reader.readAsText(e);
    //reader.readAsBinaryString(e);
    reader.onloadend = handleFileRead;
    console.log(e);
    this.setState({
      'csvData': e,
      csvFileLoaded: true,
    })



  }

  resetTable = event => {
    const { csvHeaders } = this.state;
    this.setState({
      col1: '',
      col2: '',
      col3: '',
      col4: '',
      col5: '',
      col6: '',
      col7: '',
      col8: '',
      col9: '',
      dropDownList1: csvHeaders.slice(),
      dropDownList2: csvHeaders.slice(),
      dropDownList3: csvHeaders.slice(),
      dropDownList4: csvHeaders.slice(),
      dropDownList5: csvHeaders.slice(),
      dropDownList6: csvHeaders.slice(),
      dropDownList7: csvHeaders.slice(),
      dropDownList8: csvHeaders.slice(),
      dropDownList9: csvHeaders.slice(),
      error1: false,
      error2: false,
      error3: false,
      error4: false,
      error5: false,
      error6: false,
      error7: false,
      error8: false,
      error9: false,
    });

  }

  uploadAlertClose = () => {
    this.setState({
      uploadAlert: false
    })
  };

  handleClickOpen = () => {
    this.setState({
      open: true
    })

  };

  handleClose = () => {
    this.setState({
      open: false
    })
  };


  uploadToDB = event => {
    const { col1, col2, col3, col4, col5, col6, col7, col8, col9, csvData } = this.state;

    if (col1 === '' || col2 === '' || col3 === '' || col4 === '' || col5 === '' || col6 === '' || col7 === '' || col8 === '' || col9 === '') {
      this.setState({
        uploadAlert: true,
        //allFieldsSelected: false
      })
      return;
    }

    var headers = col1 + ',' + col2 + ',' + col3 + ',' + col4 + ',' + col5 + ',' + col6 + ',' + col7 + ',' + col8 + ',' + col9;
    console.log(headers);
    console.log(this.state.csvData);
    console.log(typeof this.state.csvData);
    const formData = new FormData();
    formData.append('file', this.state.csvData, 'request.json');
    formData.append('headerList', headers);
    console.log(this.state.csvData);
    axios.post("http://localhost:8081/api/csv/upload", formData, { contentType: false })
      .then(res => {
        alert("File uploaded successfully.")
      })
  }

  viewCSV = event => {
    if (this.state.csvFileLoaded) {
      this.setState({
        showDataTable: true,
        sampleData: ""
      })
      axios.get('http://localhost:8081/api/csv/crews')
        .then(res => {
          this.setState({
            data: res.data,
            isActive: true
          })
        });


    }


  }

  render() {
    //const classes = useStyles();
    const { classes } = this.props;
    const { sampleData,data, dropDownList1, dropDownList2, dropDownList3, dropDownList4, dropDownList5, dropDownList6, dropDownList7, dropDownList8, dropDownList9, col1, col2, col3, col4, col5, col6, col7, col8, col9, showDataTable, open, uploadAlert, error1, error2, error3, error4, error5, error6, error7, error8, error9 } = this.state;
    console.log(error1);

    return (<div>
      {/* {dropDownList.length} */}
      <div className="row">
        <div className="col-md-12">
          <h4 className="heading">
            Upload Csv
        </h4>
          <p className="para">Please browse and upload the CSV file that contains the data</p>
        </div>
        <div class="container">
          <div class="row no-gutters">
            <div class="col-md-6">
              <div class="custom-file">
                {/* <input id="inputGroupFile01" type="file"  onChange={this.onFileChangeHandler} class="custom-file-input" /> */}
                <input id="inputGroupFile01" type="file" accept='.csv' onChange={e => this.onFileChangeHandler(e.target.files[0])} class="custom-file-input" />
                <label class="custom-file-label" for="inputGroupFile01"  ></label>
                {/* <p>
                  <ReactFileReader handleUploadedFiles={this.handleUploadedFiles} fileTypes={'.csv'}>
                    <button className='btn btn-primary btn-file'>Upload</button>
                  </ReactFileReader>
                </p> */}
              </div>
            </div>
            <div class="col-md-">

              <Button className="button1" onClick={this.viewCSV} >View CSV Data</Button>
            </div>
          </div>
        </div>
        <div className="col-md-11">
          {this.state.isActive ?
            <div>
              <br />
              <JsonToTable json={data} />
              <br />
            </div> : null}
        </div>
      </div>

      <TableHead>
              <TableRow>
                <TableCell style={{ width: "45%", font: "revert" }}>CSV Header</TableCell>
                <TableCell style={{ width: "46%", font: "revert" }} align="left">Model Parameter</TableCell>
                <TableCell style={{ width: "10%", font: "revert" }} align="left">CSV Example</TableCell>
              </TableRow>
      </TableHead>

            <div align="center">{sampleData}</div>       
      <div style={{ display: showDataTable ? "block" : "none" }}>

        {/* Datatable   */}
        <TableContainer >
          <Table className={classes.table} aria-label="simple table">
            {/* <TableHead>
              <TableRow>
                <TableCell style={{ width: "40%", font: "revert" }}>CSV Header</TableCell>
                <TableCell style={{ width: "30%", font: "revert" }} align="left">Model Parameter</TableCell>
                <TableCell style={{ width: "30%", font: "revert" }} align="left">CSV Example</TableCell>
              </TableRow>
            </TableHead> */}
            <TableBody>

              <TableRow >
                <TableCell >
                  <FormControl error={error1}>
                    <InputLabel id="demo-simple-select-label">{col1 === "" ? "DATE" : ""}</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      //errorMessages={['required field']}
                      value={col1}
                      onChange={this.handleChange1}
                    >
                      {dropDownList1.map((item, index) => {
                        return <MenuItem key={index} value={item}>
                          {item}
                        </MenuItem>
                      })}
                    </Select>
                    {/* <FormHelperText style={{ display: allFieldsSelected ? "none" : "block" }}>This is required!</FormHelperText> */}
                    <FormHelperText style={{ display: error1 ? "block" : "none" }}>Please map Date type value</FormHelperText>
                  </FormControl>
                </TableCell>
                <TableCell align="center">DATE</TableCell>
                <TableCell align="center">02-01-2016</TableCell>
              </TableRow>

              <TableRow >
                <TableCell >
                  <FormControl error={error2}>
                    <InputLabel id="demo-simple-select-label">{col2 === "" ? "CREW_RANK" : ""}</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={col2}
                      onChange={this.handleChange2}
                    >
                      {dropDownList2.map((item, index) => (
                        <MenuItem key={index} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                    </Select>
                    <FormHelperText style={{ display: error2 ? "block" : "none" }}>Please map string type value</FormHelperText>
                  </FormControl>
                </TableCell>
                <TableCell align="center">CREW_RANK</TableCell>
                <TableCell align="center">FS</TableCell>
              </TableRow>


              <TableRow >
                <TableCell >
                  <FormControl error={error3}>
                    <InputLabel id="demo-simple-select-label">{col3 === "" ? "BASE_STATION" : ""}</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={col3}
                      onChange={this.handleChange3}
                    >
                      {dropDownList3.map((item, index) => (
                        <MenuItem key={index} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                    </Select>
                    <FormHelperText style={{ display: error3 ? "block" : "none" }}>Please map string type value</FormHelperText>
                  </FormControl>
                </TableCell>
                <TableCell align="center">BASE_STATION</TableCell>
                <TableCell align="center">KUL</TableCell>
              </TableRow>


              <TableRow >
                <TableCell >
                  <FormControl error={error4}>
                    <InputLabel id="demo-simple-select-label">{col4 === "" ? "FLEET" : ""}</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={col4}
                      onChange={this.handleChange4}
                    >
                      {dropDownList4.map((item, index) => (
                        <MenuItem key={index} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                    </Select>
                    <FormHelperText style={{ display: error4 ? "block" : "none" }}>Please map string type value</FormHelperText>
                  </FormControl>
                </TableCell>
                <TableCell align="center">FLEET</TableCell>
                <TableCell align="center">WBA</TableCell>
              </TableRow>


              <TableRow >
                <TableCell >
                  <FormControl error={error5}>
                    <InputLabel id="demo-simple-select-label">{col5 === "" ? "PLANNED_HOME_STANDBY" : ""}</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={col5}
                      onChange={this.handleChange5}
                    >
                      {dropDownList5.map((item, index) => (
                        <MenuItem key={index} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                    </Select>
                    <FormHelperText style={{ display: error5 ? "block" : "none" }}>Please map numeric type value</FormHelperText>
                  </FormControl>
                </TableCell>
                <TableCell align="center">PLANNED_HOME_STANDBY</TableCell>
                <TableCell align="center">48</TableCell>
              </TableRow>


              <TableRow >
                <TableCell >
                  <FormControl error={error6}>
                    <InputLabel id="demo-simple-select-label">{col6 === "" ? "PLANNED_AIRPORT_STANDBY" : ""}</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={col6}
                      onChange={this.handleChange6}
                    >
                      {dropDownList6.map((item, index) => (
                        <MenuItem key={index} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                    </Select>
                    <FormHelperText style={{ display: error6 ? "block" : "none" }}>Please map numeric type value</FormHelperText>
                  </FormControl>
                </TableCell>
                <TableCell align="center">PLANNED_AIRPORT_STANDBY</TableCell>
                <TableCell align="center">34</TableCell>
              </TableRow>


              <TableRow >
                <TableCell >
                  <FormControl error={error7}>
                    <InputLabel id="demo-simple-select-label">{col7 === "" ? "UTILIZED_HOME_STANDBY" : ""}</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={col7}
                      onChange={this.handleChange7}
                    >
                      {dropDownList7.map((item, index) => (
                        <MenuItem key={index} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                    </Select>
                    <FormHelperText style={{ display: error7 ? "block" : "none" }}>Please map numeric type value</FormHelperText>
                  </FormControl>
                </TableCell>
                <TableCell align="center">UTILIZED_HOME_STANDBY</TableCell>
                <TableCell align="center">28</TableCell>
              </TableRow>


              <TableRow >
                <TableCell >
                  <FormControl error={error8}>
                    <InputLabel id="demo-simple-select-label">{col8 === "" ? "UTILIZED_AIRPORT_STANDBY" : ""}</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={col8}
                      onChange={this.handleChange8}
                    >
                      {dropDownList8.map((item, index) => (
                        <MenuItem key={index} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                    </Select>
                    <FormHelperText style={{ display: error8 ? "block" : "none" }}>Please map numeric type value</FormHelperText>
                  </FormControl>
                </TableCell>
                <TableCell align="center">UTILIZED_AIRPORT_STANDBY</TableCell>
                <TableCell align="center">20</TableCell>
              </TableRow>

              <TableRow >
                <TableCell >
                  <FormControl error={error9}>
                    <InputLabel id="demo-simple-select-label">{col9 === "" ? "DEPARTURES" : ""}</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={col9}
                      onChange={this.handleChange9}
                    >
                      {dropDownList9.map((item, index) => (
                        <MenuItem key={index} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                    </Select>
                    <FormHelperText style={{ display: error9 ? "block" : "none" }}>Please map numeric type value</FormHelperText>
                  </FormControl>
                </TableCell>
                <TableCell align="center">DEPARTURES</TableCell>
                <TableCell align="center">960</TableCell>
              </TableRow>



            </TableBody>
          </Table>
        </TableContainer>

        <div className={classes.groupButton}>
          <Button1 className={classes.reset} onClick={this.handleClickOpen} >Reset</Button1>
          <Button1 className={classes.button} onClick={this.uploadToDB}>Upload to Database</Button1>

          <Dialog
            onClose={this.uploadAlertClose}
            aria-labelledby="customized-dialog-title"
            open={uploadAlert}
          >
            <DialogTitle disableTypography id="customized-dialog-title" onClose={this.uploadAlertClose} className={classes.confmColo}>
          Error
        </DialogTitle>
            <DialogContent className={classes.dialog}>
              Please map all CSV headers!
        </DialogContent>
            <DialogActions>
              <Button autoFocus onClick={() => {
                this.uploadAlertClose();
              }} className={classes.confir} variant="contained" >
                Ok
          </Button>
            </DialogActions>
          </Dialog>


          <Dialog
            onClose={this.handleClose}
            aria-labelledby="customized-dialog-title"
            open={open}
          >
            <DialogTitle disableTypography id="customized-dialog-title" onClose={this.handleClose} className={classes.confmColo}>
              Confirm
        </DialogTitle>
            <DialogContent className={classes.dialog}>
              Are you sure you want to reset your all selection?
        </DialogContent>
            <DialogActions>
              <Button1 autoFocus onClick={this.handleClose} color="primary" className={classes.notConf}>
                Not now
          </Button1>
              <Button1 autoFocus onClick={() => {
                this.resetTable();
                this.handleClose();
              }} className={classes.confir} variant="contained" >
                Yes
          </Button1>
            </DialogActions>
          </Dialog>
        </div>
      </div>
    </div>
    );
  }
}

export default withStyles(useStyles)(Home);