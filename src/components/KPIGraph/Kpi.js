import React, { Component } from 'react';
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";

const styles = theme => ({
    root: {
        width: 170,
        height: 50,
    },
    typo: {
        font: "revert"
    },
    form: {
        position: "relative",
        top: "-28px",
        left: "50px",
    },
    table: {
        minWidth: 650,
    },
})

class KPI extends Component {
    render() {
        const {classes} =this.props;
        return <div>
            {/* <Card className={classes.root}>
                <CardContent >
                    <Typography className={classes.typo}>Graphs</Typography>
                    <FormControlLabel className={classes.form}
                        value="ON"
                        control={<Switch color="primary" font="unset" />}
                        label="ON"
                        labelPlacement="start"
                    />
                </CardContent>
            </Card> */}

            <div style={{ display: true ? "block" : "none" }}>
                <p>Table View</p>
                {/* Datatable   */}
                <TableContainer >
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell style={{ width: "28%", font: "revert" }}>Operating Date</TableCell>
                                <TableCell style={{ width: "15%", font: "revert" }} align="left">Base</TableCell>
                                <TableCell style={{ width: "15%", font: "revert" }} align="left"> Rank </TableCell>
                                <TableCell style={{ width: "15%", font: "revert" }} align="left">Fleet</TableCell>
                                <TableCell style={{ width: "15%", font: "revert" }} align="left">Actual Stand By</TableCell>
                                <TableCell style={{ width: "15%", font: "revert" }} align="left">Predicted Stand By</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>

                            <TableRow >
                                <TableCell >2020-12-07</TableCell>
                                <TableCell align="right">40</TableCell>
                                <TableCell align="right">100</TableCell>
                                <TableCell align="right">190</TableCell>
                                <TableCell align="right">35</TableCell>
                                <TableCell align="right">34</TableCell>
                            </TableRow>

                            <TableRow >
                                <TableCell >2020-12-08</TableCell>
                                <TableCell align="right">100</TableCell>
                                <TableCell align="right">200</TableCell>
                                <TableCell align="right">300</TableCell>
                                <TableCell align="right">42</TableCell>
                                <TableCell align="right">35</TableCell>
                            </TableRow>


                            <TableRow >
                                <TableCell >2020-12-09</TableCell>
                                <TableCell align="right">LS</TableCell>
                                <TableCell align="right">LS</TableCell>
                                <TableCell align="right">LS</TableCell>
                                <TableCell align="right">38</TableCell>
                                <TableCell align="right">27</TableCell>
                            </TableRow>


                            <TableRow >
                                <TableCell >2020-12-10</TableCell>
                                <TableCell align="right">KUL</TableCell>
                                <TableCell align="right">KUL</TableCell>
                                <TableCell align="right">KUL</TableCell>
                                <TableCell align="right">35</TableCell>
                                <TableCell align="right">40</TableCell>
                            </TableRow>


                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        </div>;

    }
}

export default withStyles(styles, { withTheme: true })(KPI);