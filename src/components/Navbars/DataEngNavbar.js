import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Hidden from "@material-ui/core/Hidden";
// @material-ui/icons
import Menu from "@material-ui/icons/Menu";
// core components
import AdminNavbarLinks from "./AdminNavbarLinks.js";
import RTLNavbarLinks from "./RTLNavbarLinks.js";
import Divider from "@material-ui/core/Divider";
import styles from "assets/jss/material-dashboard-react/components/headerStyle.js";
import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import { withStyles } from "@material-ui/core/styles";
import Text from 'react-text';
import { useSelector } from 'react-redux';
// import ConnIcon from "assets/img/ConnIcon.jpg";

const useStyles = makeStyles(styles);

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    marginLeft: 25,
    margin: theme.spacing(1)
  },
  switchBase: {
    padding: 1,
    "&$checked": {
      transform: "translateX(16px)",
      color: theme.palette.common.white,
      "& + $track": {
        backgroundColor: "#52d869",
        opacity: 1,
        border: "none"
      }
    },
    "&$focusVisible $thumb": {
      color: "#52d869",
      border: "6px solid #fff"
    }
  },
  thumb: {
    width: 24,
    height: 24
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px  ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[200],
    opacity: 15,
    transition: theme.transitions.create(["background-color", "border"])
  },
  checked: {},
  focusVisible: {}
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked
      }}
      {...props}
    />
  );
});

export default function Header(props) {
  const classes = useStyles();
  const isNavOpen = useSelector(state => state.leftNavOpen);

  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const [state, setState] = React.useState({
    checkedA: false,
    checkedB: true,
    checkedC: true,
  });

  const [state1, dropConn] = React.useState(true);

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

     setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen(false);
    }
  }

  function toggle(event) {
    dropConn(!state1);
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);


  function makeBrand() {
    var name;
    props.routes.map(prop => {
      if (window.location.href.indexOf(prop.layout + prop.path) !== -1) {
        name = props.rtlActive ? prop.rtlName : prop.name;
      }
      return null;
    });
    return name; 
  }
  const { color } = props;
  const appBarClasses = classNames({
    [" " + classes[color]]: color
  });
  return (
    <AppBar className={classes.appBar + appBarClasses}>
      <Toolbar className={classes.container}>
        <div className={classes.dropdow}>
      <Button
          ref={anchorRef}
          aria-controls={open ? "menu-list-grow" : undefined}
          aria-haspopup="true"
          onClick={handleToggle}
          className={classes.dropBut}
        >
          {/* <div>
          {state1 ? <span>Not Connected to Database</span> : <span>Connected to Database</span>}
          <ExpandMoreIcon />
          </div> */}
        </Button>
        <Popper
          open={open}
          anchorEl={anchorRef.current}
          role={undefined}
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom"
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList
                    autoFocusItem={open}
                    id="menu-list-grow"
                    onKeyDown={handleListKeyDown}
                    className={classes.menuItem}
                  >
                    <MenuItem>
                      <Button className={classes.dropBut}>Database Connection</Button>
                      <FormGroup className={classes.isoSwit}>
                        <FormControlLabel
                          control={
                            <IOSSwitch
                              checked={state.checkedA}
                              onChange={handleChange}
                              onClick={toggle}
                              name="checkedA"
                            />
                          }
                          // label="Toggle to ON/OFF"
                        />
                      </FormGroup>
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
        </div>
        
        {/* Here we create navbar brand, based on route name */}
        {/* <Button color="transparent" href="#" className={classes.title}>
            {makeBrand()}
          </Button> */}
        {isNavOpen ?
          (<div className={classes.flex2}>
            <span><Text><b><strong>Welcome, Nithin</strong></b><Text><br></br><small> &nbsp;&nbsp;&nbsp;&nbsp; (Data Engineer)</small></Text></Text></span>
          </div>) :
          (<div className={classes.minflex2}>
            <span><Text><b><strong>Welcome, Nithin</strong></b><Text><br></br><small> &nbsp;&nbsp;&nbsp;&nbsp; (Data Engineer)</small></Text></Text></span>
          </div>)
        }
        
        <Hidden smDown implementation="css">
          {props.rtlActive ? <RTLNavbarLinks /> : <AdminNavbarLinks />}
        </Hidden>
        
        <Hidden mdUp implementation="css">
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={props.handleDrawerToggle}
          >
            
            <Menu />
          </IconButton>
          
        </Hidden>
       
      </Toolbar>
      <div className={classes.Divide}><Divider/></div>
      <div className={classes.routeName}>
        {/* <Button color="transparent" href="#" className={classes.title}>
            {makeBrand()}
        </Button> */}
        {/* <h4><b>Analytics Platform</b></h4> */}
          </div> 
    </AppBar>
  );
}

Header.propTypes = {
  color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger"]),
  rtlActive: PropTypes.bool,
  handleDrawerToggle: PropTypes.func,
  routes: PropTypes.arrayOf(PropTypes.object)
};