import React, {Component} from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';

export default class HelloWorld extends Component {

  constructor(props) {
    super(props);

    this.state = {
      alert: 1
    };
  } 

  hideAlert() {
    console.log('Hiding alert...');
    this.setState({
      alert: 0
    });
  }

  render() {
      const {alert} = this.state;

    return (
      <div style={{ padding: '20px' }}>
          {(alert === 1)?
        <SweetAlert 
        success 
        title="Your Query has been Saved!" 
        onConfirm={() => this.hideAlert()}
      >
      </SweetAlert>: null}
      </div>
    );
  }
}