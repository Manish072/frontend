// // STEP 1 - Include Dependencies
// // Include react
// import React, {Component} from "react";
// import ReactDOM from "react-dom";

// // Include the react-fusioncharts component
// import ReactFC from "react-fusioncharts";

// // Include the fusioncharts library
// import FusionCharts from "fusioncharts";

// // Include the chart type
// import Spline from "fusioncharts/fusioncharts.charts";

// // Include the theme as fusion
// import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

// // Adding the chart and theme as dependency to the core fusioncharts
// ReactFC.fcRoot(FusionCharts, Spline, FusionTheme);

// // STEP 2 - Chart Data
// const chartData = [
//   {
//     label: "Jan",
//     value: "29"
//   },
//   {
//     label: "Feb",
//     value: "20"
//   },
//   {
//     label: "Mar",
//     value: "150"
//   },
//   {
//     label: "April",
//     value: "140"
//   },
//   {
//     label: "May",
//     value: "160"
//   },
//   {
//     label: "June",
//     value: "250"
//   },
//   {
//     label: "July",
//     value: "300"
//   },
//   {
//     label: "August",
//     value: "350"
//   }
// ];

// // STEP 3 - Creating the JSON object to store the chart configurations
// const chartConfigs = {
//   type: "spline", // The chart type
//   width: "700", // Width of the chart
//   height: "400", // Height of the chart
//   dataFormat: "json", // Data type
//   dataSource: {
//     // Chart Configuration
//     chart: {
//       //Set the chart caption
//       caption: "Model 1",
//       //Set the chart subcaption
//       subCaption: "Analysis",
//       //Set the x-axis name
//       xAxisName: "Month",
//       //Set the y-axis name
//       yAxisName: "Percentage",
//       numberSuffix: "",
//       //Set the theme for your chart
//       theme: "umber"
//     },
//     // Chart Data
//     data: chartData
//   }
// };

// // STEP 4 - Creating the DOM element to pass the react-fusioncharts component
// class Model1 extends Component {
//   render() {
//     return (<ReactFC {...chartConfigs} />);
//   }
// }

// export default Model1;

import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasGraph/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
 
class Model1 extends Component {
	render() {
		const options = {
			animationEnabled: true,
			title:{
				text: "Model 1"
			},
			axisX: {
				valueFormatString: "MMM"
			},
			axisY: {
				title: "Acuracy (in Percentage)",
				suffix: "%",
				includeZero: false
			},
			data: [{
				xValueFormatString: "MMMM",
        type: "spline",
        lineColor: 'yellow',
				dataPoints: [
					{ x: new Date(2017, 0), y: 55 },
					{ x: new Date(2017, 1), y: 57 },
					{ x: new Date(2017, 2), y: 72 },
					{ x: new Date(2017, 3), y: 32 },
					{ x: new Date(2017, 4), y: 55 },
					{ x: new Date(2017, 5), y: 33 },
					{ x: new Date(2017, 6), y: 40 },
					{ x: new Date(2017, 7), y: 52 },
					{ x: new Date(2017, 8), y: 32 },
					{ x: new Date(2017, 9), y: 42 },
					{ x: new Date(2017, 10), y: 37 },
					{ x: new Date(2017, 11), y: 38 }
				]
			}]
		}
		
		return (
		<div>
			<CanvasJSChart options = {options} 
				/* onRef={ref => this.chart = ref} */
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}

export default Model1;