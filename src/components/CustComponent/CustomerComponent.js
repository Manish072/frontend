import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from '@material-ui/core/FormControl';
import './EngineerComp.css';
import ActualModelGraph from '../Graph/ActualModel';
import Model1Graph from '../Graph/Model1';
import Model2Graph from '../Graph/Model2';
import Model from '../GraphDetailed/Model';
import Model2 from '../GraphDetailed/Model2';
import CustomerCheckBox from '../Comparisions/CustomerCheckBox';


const useStyles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      marginTop: theme.spacing(3),
      width: theme.spacing(20),
      height: theme.spacing(6),
      marginRight: theme.spacing(2),
      marginBottom: theme.spacing(3)
    }
  },
});

class EngineerComponent extends Component {

    constructor(props) {
        super(props);
        this.state = { status: 1 ,
        DataEngineer1: 2,
        Customer1: 4,
      }; // 0: no show, 1: show yes, 2: show no.
      }

      radioHandler = (status) => {
        this.setState({status});
      }

render(){
const {classes} = this.props;
const { status } = this.state;

  return (
      <div>
    {/* <div className={classes.root}>
      <Paper elevation={3}>
          <div className="radio">
      <RadioGroup
          row
          aria-label="position"
          name="position"
          defaultValue="top"
          
        >
          <FormControlLabel
            value="Actual Model"
            control={<Radio color="primary"
            checked={status === 1}
            onClick={(e) => this.radioHandler(1)} />}
            label="Model 1"
          />
        </RadioGroup>
        </div>
      </Paper>
      <Paper elevation={3}>
      <div className="radio">
      <RadioGroup
          row
          aria-label="position"
          name="position"
          defaultValue="top"
         >
          <FormControlLabel
            value="Model 1"
            control={<Radio color="primary" 
            checked={status === 2}
            onClick={(e) => this.radioHandler(2)}/>}
            label="Model 2"
          />
        </RadioGroup>
        </div>
      </Paper>
      <Paper elevation={3}>
      <div className="radio">
      <RadioGroup
          row
          aria-label="position"
          name="position"
          defaultValue="top"
        >
          <FormControlLabel
            value="Model 3"
            control={<Radio color="primary" 
            checked={status === 3}
            onClick={(e) => this.radioHandler(3)}/>}
            label="Model 3"
          />
        </RadioGroup>
        </div>
      </Paper>
    </div> */}
    {/* {(status === 1)?<Model/>:null}
    {(status === 2)?<Model2/>:null}
    {(status === 3)?<Model2Graph/>:null} */}
    <p><CustomerCheckBox {...this.state}/></p>
    {console.log(this.props.workDays),
    console.log(this.props.workDays)}

    </div>
  );
  }
}

export default withStyles(useStyles)(EngineerComponent)