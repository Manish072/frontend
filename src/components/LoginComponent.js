import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { withStyles } from '@material-ui/core/styles';
import crew from '../../src/assets/img/Crewcustomerchina.jpg';
// import crewLogo from '../../src/assets/img/logo_transparent.png';
import crewLogo from '../../src/assets/img/Emblem1.png';
import ibsLogo from '../../src/assets/img/China-Airlines-logo.png';
import ButtonBase from '@material-ui/core/ButtonBase';
import Grid from '@material-ui/core/Grid';
import Notifier, { openSnackbar } from './Notifier';
import './LoginComponent.css';

const useStyles = theme => ({
    root: {
        width:'auto',
        height: 'auto'
    },
    but: {
        margin: 'auto',
        marginTop: theme.spacing(1),
        // width:210,
        fontFamily: "Roboto",
        fontWeight: "normal",
        fontSize: "18px",
        width: "298px",
        height: "40px",
        textTransform: "none",
        backgroundColor: "#004162"
    },
    paper: {
        margin: 'auto',
        backgroundColor: 'white'
      },
    image: {
        width: 'auto',
        height: 'auto',
      },
    crewLogo: {
        width: 60,
        height: 60,
        marginLeft: theme.spacing(15)
    },
    ibslogo: {
        width:200,
        marginTop: theme.spacing(24),
        marginLeft: theme.spacing(-1) 
    },
    copyright: {
        width:270,
        marginTop: theme.spacing(1),
        marginLeft: theme.spacing(0),
        fontSize: "12px"
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%'
      },
    form: {
       margin: 'auto',
       marginTop: theme.spacing(13)
    },
    margin: {
        margin: theme.spacing.unit,
      },
    userName:{
        width: "298px",
        height: "40px",
        fontFamily: "Roboto",
        fontSize: "14px",
        fontWeight: "normal",
        borderRadius: "3px",
        //border: "solid 1px #707070",
        backgroundColor: "#ffffff"
    },
    passWord:{
        width: "298px",
        height: "40px",
        // border: "solid 1px #707070",
        backgroundColor: "#ffffff"
    }  
  });

function Copyright() {
    return (
      <Typography variant="body2" color="textSecondary">
        {'© '}
        <Link color="inherit" href="https://www.ibsplc.com/">
          2020 ibssoftware. All Rights Reserved.
        </Link>{' '}
        {/* {new Date().getFullYear()} */}
        {/* {'.'} */}
      </Typography>
    );
  }

class LoginComponent extends Component {
    
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            hasLoginFailed: false,
            showSuccessMessage: false
        }

        this.handleChange = this.handleChange.bind(this)
    }

    handleChange = (event) => {
        this.setState(
            {
                [event.target.name]
                    : event.target.value
            }
        )
    }

    showNotifier = (event) => {
        event.preventDefault();
        if((this.state.username)!='DataScientist' || (this.state.password)!='DataScientist' || (this.state.username)!='DataEngineer' || (this.state.password)!='DataEngineer' ) {
            openSnackbar({ message: 'Invalid Credentials' });
        }

        if(this.state.username==='DataScientist' && this.state.password==='DataScientist'){
            this.props.history.push(`/admin/table/:tabId`)
            this.setState({showSuccessMessage:true})
            this.setState({hasLoginFailed:false})
        }

        if(this.state.username==='DataEngineer' && this.state.password==='DataEngineer'){
            this.props.history.push(`/deploy/dataEng`)
            this.setState({showSuccessMessage:true})
            this.setState({hasLoginFailed:false})
        }

        else {
            this.setState({showSuccessMessage:false})
            this.setState({hasLoginFailed:true})
        }
        
      }

    render() {

        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Grid className={classes.paper}>
                    <Grid container>
                        <Grid item>
                            <ButtonBase className={classes.image}>
                                    <img className={classes.img} alt="complex" src={crew} />
                            </ButtonBase>
                        </Grid>
                        <Grid item xs={12} sm container>
                            <Grid item xs container direction="column">
                                <Grid item className={classes.form}>
                                                    <Notifier/>
                                                    <ButtonBase className={classes.crewLogo}>
                                                        <img className={classes.img} src={crewLogo} alt="logo"/>
                                                    </ButtonBase>
                                                    <h4 className="heading1">
                                                        Crew Analytics
                                                    </h4>
                                                    <h5 className="signIn">
                                                        Sign in to Crew Analytics
                                                    </h5>
                                                    <form onSubmit={this.showNotifier} Validate>
                                                    <Grid item>
                                                    <TextField
                                                        variant="outlined"
                                                        margin="normal"
                                                        required
                                                        type="text"
                                                        name="username"
                                                        value={this.state.username}
                                                        onChange={this.handleChange}
                                                        label="User Name"
                                                        autoComplete="off"
                                                        autoFocus
                                                        size="small"
                                                        className={classes.userName}
                                                    />
                                                    </Grid>
                                                    <Grid item>
                                                    <TextField
                                                        variant="outlined"
                                                        margin="normal"
                                                        required
                                                        name="password"
                                                        value={this.state.password}
                                                        onChange={this.handleChange}
                                                        label="Password"
                                                        type="password"
                                                        id="password"
                                                        autoComplete="off"
                                                        size="small"
                                                        className={classes.passWord}
                                                    />
                                                    </Grid>
                                                    <Grid Item>
                                                    <Button
                                                        style={{
                                                            backgroundColor: "#004162",
                                                        }}
                                                        type="submit"
                                                        variant="contained"
                                                        color="primary"
                                                        className={classes.but}
                                                        
                                                    >
                                                        Sign in
                                                    </Button>
                                                    </Grid>
                                                </form>
                                                <ButtonBase className={classes.ibslogo}>
                                                    <img className={classes.img} src={ibsLogo} alt="logo"/>
                                                </ButtonBase>
                                                <Typography className={classes.copyright}>
                                                    <Copyright />
                                                </Typography>
                                    </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withStyles(useStyles)(LoginComponent)