/*eslint-disable*/
import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
// core components
import styles from "assets/jss/material-dashboard-react/components/footerStyle.js";
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { Router, Route, Switch, Redirect, BrowserRouter, Link } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { simulation, deployment, dbConfig } from '../../actions';

const useStyles = makeStyles(styles);

var style = {
  backgroundColor: "#F8F8F8",
  borderTop: "1px solid #E7E7E7",
  padding: "20px",
  position: "fixed",
  left: "0",
  bottom: "0",
  height: "60px",
  width: "100%",
}

var phantom = {
  display: 'block',
  padding: '20px',
  height: '60px',
  width: '100%',
}
export default function Footer() {
  const classes = useStyles();
  const value = useSelector(state => state.openTab);
  const dispatch = useDispatch();
  const isNavOpen = useSelector(state => state.leftNavOpen);

  return (
    // <footer className={classes.footer}>
    //   <div className={classes.container}>
    <div>
      <div style={phantom}>
        <div style={style}>
          {
            <div>
              {value === 0 ?
                <div>
                  <Link onClick={() => dispatch(simulation())}>
                    <span className={classes.footertxt} style={{ marginLeft: '89.5%' }}>Model Evaluation<ArrowForwardIosIcon /></span>
                  </Link>
                </div>
                : (value === 1) ?

                  <div>

                    <Link onClick={() => dispatch(dbConfig())} >
                      {isNavOpen ? (<span className={classes.footertxt} style={{ margin: '17%' }}><ArrowBackIosIcon />Database Configuration</span>) :
                        (<span className={classes.footertxt} style={{ margin: '5%' }}><ArrowBackIosIcon />Database Configuration</span>)}
                    </Link>


                    {/* <Link onClick={() => dispatch(deployment())}>
                          <span className={classes.footertxt} style={{paddingLeft:'37%'}} >Model Deployment <ArrowForwardIosIcon /></span>
                      </Link> */}



                  </div>
                  :
                  <div>
                    <Link onClick={() => dispatch(simulation())}>
                      <span className={classes.footertxt} style={{ margin: '18%' }}><ArrowBackIosIcon />Model Simulation </span>
                    </Link>
                  </div>
              }
            </div>

          }

        </div>
      </div>
    </div>
    //     {
    //       // <h4 align="right">Model Simulation<ArrowForwardIosIcon/></h4>
    //       //    <ul>
    //       //    <li><Link to="/simulation/">Model Simulation<ArrowForwardIosIcon/></Link></li>

    //       //  </ul>


    //       //  <Link to="/simulation/1">Model Simulation<ArrowForwardIosIcon/></Link>
    //       <div>
    //         {value === 0 ?
    //           <div align="right">
    //             <Link onClick={() => dispatch(simulation())}>
    //               <span className={classes.footertxt}>Model Simulation <ArrowForwardIosIcon /></span>
    //             </Link>
    //           </div>
    //           : (value === 1) ?

    //             <div>

    //                   <Link onClick={() => dispatch(dbConfig())} >
    //                       <span className={classes.footertxt} ><ArrowBackIosIcon />Database Configuration</span>
    //                   </Link>


    //                   <Link onClick={() => dispatch(deployment())} style={{paddingLeft:'64%'}}>
    //                       <span className={classes.footertxt}  >Model Deployment <ArrowForwardIosIcon /></span>
    //                   </Link>



    //             </div>
    //             : 
    //             <div align="left">
    //                <Link onClick={() => dispatch(simulation())}>
    //                    <span className={classes.footertxt}><ArrowBackIosIcon />Model Simulation </span>
    //                </Link>
    //             </div>
    //           }
    //       </div>

    //     /* <div className={classes.left}>
    //       <List className={classes.list}>
    //         <ListItem className={classes.inlineBlock}>
    //           <a href="#home" className={classes.block}>
    //             Home
    //           </a>
    //         </ListItem>
    //         <ListItem className={classes.inlineBlock}>
    //           <a href="#company" className={classes.block}>
    //             Company
    //           </a>
    //         </ListItem>
    //         <ListItem className={classes.inlineBlock}>
    //           <a href="#portfolio" className={classes.block}>
    //             Portfolio
    //           </a>
    //         </ListItem>
    //         <ListItem className={classes.inlineBlock}>
    //           <a href="#blog" className={classes.block}>
    //             Blog
    //           </a>
    //         </ListItem>
    //       </List>
    //     </div> */}
    //     {/* <p className={classes.right}>
    //       <span>
    //         &copy; {1900 + new Date().getYear()}{" "}
    //         <a
    //           href="https://www.ibsplc.com/"
    //           target="_blank"
    //           className={classes.a}
    //         >
    //           IBS Software
    //         </a>
    //         . All Rights Reserved.
    //       </span>
    //     </p> */}
    //   </div>
    // </footer>
  );
}
