import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Paper from '@material-ui/core/Paper';
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from '@material-ui/core/FormControl';
import Switch from "@material-ui/core/Switch";
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import Comparision from '../../components/ComparisonGraph/Comparision';
import Predictcsv from '../../views/Predict/Predictcsv';
import './CheckBoxSelection.css'
import { FormGroup } from '@material-ui/core';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormHelperText from '@material-ui/core/FormHelperText';
import CloudDownloadOutlinedIcon from '@material-ui/icons/CloudDownloadOutlined';
import Tooltip from '@material-ui/core/Tooltip';
import csvLogo from '../../assets/img/CSV_Icon.png';
import ButtonBase from '@material-ui/core/ButtonBase';


const useStyles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      marginTop: theme.spacing(1),
      width: theme.spacing(20),
      height: theme.spacing(6),
      marginRight: theme.spacing(2),
      marginBottom: theme.spacing(3),
      marginLeft: theme.spacing(2)
    },
  },
  formControl: {
    marginLeft: theme.spacing(6),
    width: theme.spacing(21),
  },
  selectEmpty: {
    marginTop: theme.spacing(0.5),
  },
  CsvIcon: {
    marginTop: theme.spacing(2.5),
    paddingLeft: theme.spacing(2)
  },
  check: {
    paddingLeft: theme.spacing(2),
  },
  check2: {
    paddingLeft: theme.spacing(0),
  },
  check3: {
    paddingLeft: theme.spacing(2),
  },
  check4: {
    paddingLeft: theme.spacing(2)
  },
  root1: {
    '&:hover': {
      backgroundColor: 'transparent',
    }
  },
  label: {
    width: theme.spacing(28),
    color: 'Black'
  },
  label1: {
    color: '#403a3a',
    width: theme.spacing(10),
  },
  icon: {
    width: theme.spacing(2),
    height: theme.spacing(2),
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: 'none'
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#004162',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: theme.spacing(2),
      height: theme.spacing(2),
      backgroundImage:
        "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
        " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
        "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: '#004162',
    },
  },
  card: {
    marginLeft: theme.spacing(7),
    marginTop: theme.spacing(3)

  },
  typo: {
    font: "revert",
    marginRight: theme.spacing(10)
  },
  form: {
    position: "relative",
    top: "-30px",
    marginLeft: theme.spacing(10)
  },
  csvlogo: {
    width: 60,
    marginTop: theme.spacing(1.5),
    marginLeft: theme.spacing(5)
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '50%',
    maxHeight: '50%'
  },
});

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 32,
    height: 20,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: '#52d869',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: '#52d869',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 18,
    height: 18,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});

const useStylesBootstrap = makeStyles((theme) => ({
  arrow: {
    color: theme.palette.common.black,
  },
  tooltip: {
    backgroundColor: theme.palette.common.black,
  },
}));
// Inspired by blueprintjs

function BootstrapTooltip(props) {
  const classes = useStylesBootstrap();

  return <Tooltip arrow classes={classes} {...props} />;
}

class CheckBoxSelection extends Component {

  constructor(props) {
    super(props);
    this.handleParameters = this.handleParameters.bind(this);
    this.state = {
      status: 3,
      workDays: [],
      statusActual: 1,
      toggleStatus: 'Off',
      parameters: '',
      model1: 1,
      model2: 3,
      model3: 5,
      DataScientist: 10,
      CustomerStatus: this.props.Customer1
    };
    // 0: no show, 1: show yes, 2: show no.
  }

  radioHandler = (status) => {
    this.setState({ status });
  }

  radioHandlerActual = (statusActual) => {
    this.setState({ statusActual });
  }

  handleCheckboxChange = (event) => {
    let newArray = [...this.state.workDays, event.target.id];
    if (this.state.workDays.includes(event.target.id)) {
      newArray = newArray.filter((day) => day !== event.target.id);
    }
    this.setState({
      workDays: newArray
    });
  };

  handleStatusModel1Change() {
    if (this.state.model1 == 1) {
      this.setState({
        model1: 2
      })
    }
    else {
      this.setState({
        model1: 1
      })
    }
  };

  handleStatusModel2Change() {
    if (this.state.model2 == 3) {
      this.setState({
        model2: 4
      })
    }
    else {
      this.setState({
        model2: 3
      })
    }
  };

  handleStatusModel3Change() {
    if (this.state.model3 == 5) {
      this.setState({
        model3: 6
      })
    }
    else {
      this.setState({
        model3: 5
      })
    }
  };


  twoCalls = (event) => {
    this.handleCheckboxChange(event);
    this.handleStatusModel1Change();
  };

  Model2Calls = (event) => {
    this.handleCheckboxChange(event);
    this.handleStatusModel2Change();
  };

  Model3Calls = (event) => {
    this.handleCheckboxChange(event);
    this.handleStatusModel3Change();
  }

  handleToggle = () => {
    if (this.state.toggleStatus == 'Off')
      this.setState({
        toggleStatus: 'On'
      })
    else {
      this.setState({
        toggleStatus: 'Off'
      })
    }
  }

  handleParameters(event) {
    var name = event.target.name;
    this.setState({
      ...this.state,
      [name]: event.target.value,
    });
  }

  render() {
    const { classes } = this.props;
    const { status } = this.state;

    return (
      <div className="row">
        <div>
          <h4 className="heading2">
          Model Evaluation
            </h4>
          <p className="para2">Here you could compare the predicted standby count from the ML model and the actual standby count collected after the day of ops to evaluate the accuracy  of the solution.</p>
        </div>
        <FormGroup>
          <div className={classes.root}>
            {/* <RadioGroup
          row
          aria-label="position"
          name="position"
          defaultValue="top"
          
        >
          <FormControlLabel
            value="Actual Model"
            control={<Radio color="primary"
            checked={status === 1}
            onClick={(e) => this.radioHandler(1)} />}
            label="Model 1"
          />
        </RadioGroup> */}
            <FormControlLabel
              className={classes.label}
              control={<Checkbox
                className={classes.check}
                style={{
                  color: "grey",
                }}
                defaultChecked
                disabled
                id="1"
                value="Actual Model"
                onChange={this.handleCheckboxChange}
                onClick={(e) => this.radioHandlerActual(1)}
              />}
              label="Actual Standby Count"
            />
            {/* <RadioGroup
          row
          aria-label="position"
          name="position"
          defaultValue="top"
         >
          <FormControlLabel
            value="Model 1"
            control={<Radio color="primary" 
            checked={status === 2}
            onClick={(e) => this.radioHandler(2)}/>}
            label="Model 2"
          />
        </RadioGroup> */}
            <FormControlLabel
              className={classes.label1}
              control={<Checkbox
                className={classes.check2}
                style={{
                  color: "#004162",
                }}
                id="Model1"
                value="LSTM"
                onChange={this.twoCalls}
                onClick={(e) => this.radioHandler(2)}
              />}
              label="LSTM"
            />
            {/* <RadioGroup
          row
          aria-label="position"
          name="position"
          defaultValue="top"
        >
          <FormControlLabel
            value="Model 2"
            control={<Radio color="primary" 
            checked={status === 3}
            onClick={(e) => this.radioHandler(3)}/>}
            label="Model 3"
          />
        </RadioGroup> */}
            {(this.props.status == undefined) ?
              <FormControlLabel
                className={classes.label1}
                control={<Checkbox
                  className={classes.check3}
                  style={{
                    color: "#004162",
                  }}
                  id="Model2"
                  value="CNN"
                  onChange={this.Model2Calls}
                  onClick={(e) => this.radioHandler(3)}
                />}
                label="CNN"
              /> : null}
            {console.log(this.props.DataEngineer)}
            {console.log(this.props.DataScientist)}
            {console.log(this.props.Customer)}
            {console.log(this.props.username)}
            {console.log(this.props.status)}
            {(this.props.status !== undefined) && (this.props.status !== 1) || (this.props.scientist == 8) ?
              <FormControlLabel
                className={classes.label1}
                control={<Checkbox
                  className={classes.check4}
                  style={{
                    color: "#004162",
                  }}
                  id="Model3"
                  value="GRU"
                  onChange={this.Model3Calls}
                  onClick={(e) => this.radioHandler(4)}
                />}
                label="GRU"
              /> : null}

            {/* <FormControl className={classes.formControl}>
              <NativeSelect
                defaultValue={"Stand By Count"}
                className={classes.selectEmpty}
                value={this.state.parameters}
                name="parameters"
                onChange={this.handleParameters}
                inputProps={{ 'aria-label': 'parameters' }}
              >
                <option value="Stand By Count">Stand By Count</option>
                <option value="MAPE Error">MAPE Error</option>
                <option value="Custome Error">Custom Error</option>
                <option value="Error Rate">Error Rate</option>
              </NativeSelect>
              <FormHelperText>Parameters</FormHelperText>
            </FormControl> */}
            <div className={classes.card}>
              <Typography className={classes.typo}>Table View</Typography>
              <FormControlLabel className={classes.form}
                value={this.state.toggleStatus}
                control={<IOSSwitch onChange={this.handleToggle} />}
                label={this.state.toggleStatus}
                labelPlacement="start"
              />
            </div>
            {/* <BootstrapTooltip title="Download As CSV">

      <CloudDownloadOutlinedIcon style={{marginTop:8,width: 28}}/>
      </BootstrapTooltip> */}
            <ButtonBase className={classes.csvlogo}>
              <img className={classes.img} src={csvLogo} />
            </ButtonBase>
          </div>
        </FormGroup>
        {/* {(status === 1)?<Model/>:null}
    {(status === 2)?<Model2/>:null}
    {(status === 3)?<Model2Graph/>:null} */}
        <Comparision {...this.state} />
        {/* <Predictcsv {...this.state}/> */}
      </div>
    );
  }
}

export default withStyles(useStyles)(CheckBoxSelection)