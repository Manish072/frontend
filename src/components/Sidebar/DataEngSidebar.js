/*eslint-disable*/
import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
// @material-ui/core components
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import styles from "assets/jss/material-dashboard-react/components/sidebarStyle.js";
import './Sidebar.css';
import { leftNavOpen } from '../../actions';
import { useDispatch } from "react-redux";
import { FiChevronsLeft,FiChevronsRight } from 'react-icons/fi';

const useStyles = makeStyles(styles);

export default function DataEngSidebar(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const dispatch = useDispatch();

  const handleDrawerOpen = () => {
    dispatch(leftNavOpen(true));
    setOpen(true);
  };

  const handleDrawerClose = () => {
    dispatch(leftNavOpen(false));
    setOpen(false);
  };

  // verifies if routeName is the one active (in browser input)
  function activeRoute(routeName) {
    return window.location.href.indexOf(routeName) > -1 ? true : false;
  }
  const { color, logo, iblogo, ibText, image, logoText, routesDataEng } = props;
  var links = (
    <List className={classes.list}>
      {routesDataEng.map((prop, key) => {
        var activePro = " ";
        var listItemClasses;
        if (prop.path === "/upgrade-to-pro") {
          activePro = classes.activePro + " ";
          listItemClasses = classNames({
            [" " + classes[color]]: true
          });
        } else {
          listItemClasses = classNames({
            [" " + classes[color]]: activeRoute(prop.layout + prop.path)
          });
        }
        const whiteFontClasses = classNames({
          [" " + classes.whiteFont]: activeRoute(prop.layout + prop.path)
        });
        return (
          <NavLink
            to={prop.layout + prop.path}
            className={activePro + classes.item}
            activeClassName="active"
            key={key}
          >
            <ListItem button className={classes.itemLink + listItemClasses}>
              {typeof prop.icon === "string" ? (
                <Icon
                  className={classNames(classes.itemIcon, whiteFontClasses, {
                    [classes.itemIconRTL]: props.rtlActive
                  })}
                >
                  {prop.icon}
                </Icon>
              ) : (open ?
                (<prop.icon
                  className={classNames(classes.itemIcon, whiteFontClasses, {
                    [classes.itemIconRTL]: props.rtlActive
                  })}
                />) :
                (<prop.icon
                  className={classNames(classes.minItemIcon, whiteFontClasses, {
                    [classes.itemIconRTL]: props.rtlActive,
                    [classes.minItemIcon]: !open
                  })}
                />)
                )}
              <ListItemText
                // primary={props.rtlActive ? prop.rtlName : prop.name}
                primary={open ? prop.name : ""}
                className={classNames(classes.itemText, whiteFontClasses, {
                  [classes.itemTextRTL]: props.rtlActive
                })}
                disableTypography={true}
              />
            </ListItem>
          </NavLink>
        );
      })}
    </List>
  );
  var brand = (
    <div className={classes.logo}>
      <div className={classes.logoImage}>
        <img src={logo} alt="logo" className={classes.img} />
      </div>
      <h4 className="logoText">
        {logoText}
      </h4>
    </div>
  );

  var minbrand = (
    <div className={classes.minlogo}>
      <div className={classes.logoImage}>
        <img src={logo} alt="logo" className={classes.img} />
      </div>
    </div>
  );

  var ibslogo = (
    <div className={classes.iblogo}>
      <div className={classes.logoImage}>
        <img src={iblogo} />
      </div>
      <div className={classes.ibText}>
        {ibText}
      </div>
    </div>
  );

  return (
    <div>
      <Hidden smDown implementation="css">
        {<Drawer
          anchor={props.rtlActive ? "right" : "left"}
          variant="permanent"
          open
          classes={{
            paper: classNames(classes.drawerPaper, {
              [classes.drawerPaperRTL]: props.rtlActive,
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open
            })
          }}
        >
          {open ? brand : minbrand}
          <div className={classes.sidebarWrapper}>{links}</div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundColor: "#0080ff" }}
            />
          ) : null}
           {open ? ibslogo : ""}

           <div>
            <IconButton>
              {open ?
                (<FiChevronsLeft className={classes.ChevronIcon} onClick={handleDrawerClose} />)
                :
                (<FiChevronsRight className={classes.ChevronIcon} onClick={handleDrawerOpen} />)}
            </IconButton>
          </div>

        </Drawer>}
      </Hidden>
    </div>
  );
}

DataEngSidebar.propTypes = {
  rtlActive: PropTypes.bool,
  handleDrawerToggle: PropTypes.func,
  bgColor: PropTypes.oneOf(["purple", "blue", "green", "orange", "red"]),
  logo: PropTypes.string,
  image: PropTypes.string,
  logoText: PropTypes.string,
  ibslogo: PropTypes.string,
  routesDataEng: PropTypes.arrayOf(PropTypes.object),
  open: PropTypes.bool
};