/*eslint-disable*/
import React from "react";
import clsx from "clsx";
import classNames from "classnames";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
// @material-ui/core components
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Icon from "@material-ui/core/Icon";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import CssBaseline from "@material-ui/core/CssBaseline";
import ListItemIcon from "@material-ui/core/ListItemIcon";

import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
// core components
import AdminNavbarLinks from "components/Navbars/AdminNavbarLinks.js";
import RTLNavbarLinks from "components/Navbars/RTLNavbarLinks.js";

import styles from "assets/jss/material-dashboard-react/components/sidebarStyle.js";
import './Sidebar.css';
import { leftNavOpen } from '../../actions';
import { useDispatch } from "react-redux";
import { FiChevronsLeft,FiChevronsRight } from 'react-icons/fi';

const useStyles = makeStyles(styles);

export default function Sidebar(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const dispatch = useDispatch();

  const handleDrawerOpen = () => {    
    dispatch(leftNavOpen(true));
    setOpen(true);
  };

  const handleDrawerClose = () => {
    dispatch(leftNavOpen(false));
    setOpen(false);
  };

  // verifies if routeName is the one active (in browser input)
  function activeRoute(routeName) {
    return window.location.href.indexOf(routeName) > -1 ? true : false;
  }
  const { color, logo, iblogo, ibText, image, logoText, routes } = props;
  var links = (
    <List className={classes.list}>
      {routes.map((prop, key) => {
        var activePro = " ";
        var listItemClasses;
        if (prop.path === "/upgrade-to-pro") {
          activePro = classes.activePro + " ";
          listItemClasses = classNames({
            [" " + classes[color]]: true
          });
        } else {
          listItemClasses = classNames({
            [" " + classes[color]]: activeRoute(prop.layout + prop.path)
          });
        }
        const whiteFontClasses = classNames({
          [" " + classes.whiteFont]: activeRoute(prop.layout + prop.path)
        });
        return (
          <NavLink
            to={prop.layout + prop.path}
            className={activePro + classes.item}
            activeClassName="active"
            key={key}
          >
            <ListItem button className={classes.itemLink + listItemClasses}>
              {typeof prop.icon === "string" ? (
                <Icon
                  className={classNames(classes.itemIcon, whiteFontClasses, {
                    [classes.itemIconRTL]: props.rtlActive
                  })}
                >
                  {prop.icon}
                </Icon>
              ) : (open ?
                (<prop.icon
                  className={classNames(classes.itemIcon, whiteFontClasses, {
                    [classes.itemIconRTL]: props.rtlActive
                  })}
                />) :
                (<prop.icon
                  className={classNames(classes.minItemIcon, whiteFontClasses, {
                    [classes.itemIconRTL]: props.rtlActive,
                    [classes.minItemIcon]: !open
                  })}
                />)
                )}
              <ListItemText
                // primary={props.rtlActive ? prop.rtlName : prop.name}
                primary={open ? prop.name : ""}
                className={classNames(classes.itemText, whiteFontClasses, {
                  [classes.itemTextRTL]: props.rtlActive
                })}
                disableTypography={true}
              />
            </ListItem>
          </NavLink>
        );
      })}
    </List>
  );
  var brand = (
    <div className={classes.logo}>
      <div className={classes.logoImage}>
        <img src={logo} alt="logo" className={classes.img} />
      </div>
      <h4 className="logoText">
        {logoText}
      </h4>
    </div>
  );

  var minbrand = (
    <div className={classes.minlogo}>
      <div className={classes.logoImage}>
        <img src={logo} alt="logo" className={classes.img} />
      </div>
    </div>
  );

  var ibslogo = (
    <div className={classes.iblogo}>
      <div className={classes.logoImage}>
        <img src={iblogo} />
      </div>
      <div className={classes.ibText}>
        {ibText}
      </div>
    </div>
  );

  return (
    //<div>
    // {
    // <div className={classes.root}>
    // <CssBaseline />
    // <AppBar
    //    position="fixed"
    //   className={clsx(classes.appBar, {
    //     [classes.appBarShift]: open
    //   })}
    // >
    //   <Toolbar>
    //     <IconButton
    //       color="inherit"
    //       aria-label="open drawer"
    //       onClick={handleDrawerOpen}
    //       edge="start"
    //       className={clsx(classes.menuButton, {
    //         [classes.hide]: open
    //       })}
    //     >
    //       <MenuIcon />
    //     </IconButton>
    //     </Toolbar>
    // </AppBar>
    // <Drawer
    //   variant="permanent"
    //   className={clsx(classes.drawer, {
    //     [classes.drawerOpen]: open,
    //     [classes.drawerClose]: !open
    //   })}
    //   classes={{
    //     paper: clsx({
    //       [classes.drawerOpen]: open,
    //       [classes.drawerClose]: !open
    //     })
    //   }}
    // >
    //   <div className={classes.toolbar}>
    //     <IconButton onClick={handleDrawerClose}>
    //       {theme.direction === "rtl" ? (
    //         <ChevronRightIcon />
    //       ) : (
    //         <ChevronLeftIcon />
    //       )}
    //     </IconButton>
    //   </div>
    //   {brand}
    //     <div className={classes.sidebarWrapper}>{links}</div>
    //     {image !== undefined ? (
    //       <div
    //         className={classes.background}
    //         //style={{ backgroundColor: "#0080ff" }}
    //       />
    //     ) : null}
    //     {ibslogo}
    //   {/* <List>
    //     {["Inbox", "Starred", "Send email", "Drafts"].map((text, index) => (
    //       <ListItem button key={text}>
    //         <ListItemIcon>
    //           {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
    //         </ListItemIcon>
    //         <ListItemText primary={text} />
    //       </ListItem>
    //     ))}
    //   </List> */}
    // </Drawer>
    //</div>

    <div>
      <Hidden smDown implementation="css">
        {<Drawer
          anchor={props.rtlActive ? "right" : "left"}
          variant="permanent"
          open
          classes={{
            paper: classNames(classes.drawerPaper, {
              [classes.drawerPaperRTL]: props.rtlActive,
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open
            })
          }}
        >
          {open ? brand : minbrand}
          <div className={classes.sidebarWrapper}>{links}</div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundColor: "#0080ff" }}
            />
          ) : null}
          {open ? ibslogo : ""}

          <div>
            <IconButton>
              {open ?
                (<FiChevronsLeft className={classes.ChevronIcon} onClick={handleDrawerClose} />)
                :
                (<FiChevronsRight className={classes.ChevronIcon} onClick={handleDrawerOpen} />)}
            </IconButton>
          </div>

        </Drawer>}
      </Hidden>
    </div>
  );
}

Sidebar.propTypes = {
  rtlActive: PropTypes.bool,
  handleDrawerToggle: PropTypes.func,
  bgColor: PropTypes.oneOf(["purple", "blue", "green", "orange", "red"]),
  logo: PropTypes.string,
  image: PropTypes.string,
  logoText: PropTypes.string,
  ibslogo: PropTypes.string,
  routes: PropTypes.arrayOf(PropTypes.object),
  open: PropTypes.bool
};
