import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from '@material-ui/core/FormControl';
import './RadioSelection.css';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import axios from "axios";
import './FormUser.css';
import { main } from '@popperjs/core';
import SaveQuery from './SaveQuery';
import NewQuery from './NewQuery';
import { JsonToTable } from "react-json-to-table";
import Table from 'react-bootstrap/Table'
import { CsvToHtmlTable } from 'react-csv-to-table';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import theme from './Theme';


const useStyles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      marginTop: theme.spacing(1),
      width: theme.spacing(20),
      height: theme.spacing(6),
      marginRight: theme.spacing(2),
      marginBottom: theme.spacing(1)
    }
  },
  input: {
    height: 45,
    width: 300
  },
  input2: {
    height: 95,
    width: 1000
  },
  button: {
    // marginLeft: theme.spacing(109),
    // marginTop: theme.spacing(5),
    marginLeft: theme.spacing(4),
    boxShadow: '1000',
    textTransform: 'none',
    fontSize: 16,
    color: 'white',
    padding: '6px 12px',
    // border: '1px solid',
    lineHeight: 1.95,
    backgroundColor: '#c73232',
    borderColor: '#c73232',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#c73232',
      borderColor: '#c73232',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#c73232',
      borderColor: '#c73232',
    },
    '&:focus': {
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      outline: "none"
    },
  },
  save: {
    marginLeft: theme.spacing(4),
    boxShadow: '1000',
    textTransform: 'none',
    fontSize: 16,
    color: '#004162',
    padding: '6px 12px',
    border: '1px solid',
    lineHeight: 1.95,
    backgroundColor: 'white',
    borderColor: 'black',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: 'white',
      borderColor: 'black',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: 'white',
      borderColor: 'black',
    },
    '&:focus': {
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      outline: "none"
    },
  },
  groupButton: {  //newQuery
    marginTop: theme.spacing(5),
    paddingLeft: theme.spacing(79)
  },
  groupButton2: {  //saveQuery
    marginTop: theme.spacing(5),
    paddingLeft: theme.spacing(60)
  },
  jsontble: {
    width: 1100,
    marginLeft: 9
  },
  rootsnack: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
      // marginBottom: theme.spacing(50)
    },
  },
  snackbarStyleViaContentProps: {
    backgroundColor: "#12805d"
  }
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


export class FormPersonalDetails extends Component {
  constructor(props) {
    super(props);
    this.state = { status: 1, isActive: false, data: '', open: false, IsQueryInValid: false }; // 0: no show, 1: show yes, 2: show no.
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({
      open: false
    })
  };
  radioHandler = (status) => {
    this.setState({ status });
    if (status == 2) {
      this.setState({
        isActive: false
      })
    };
  }



  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  handleSubmit = (event) => {
    event.preventDefault();
    if (this.props.values.isChecked && this.props.values.userName !== "") {
      localStorage.firstName = this.props.values.userName
      localStorage.checkbox = this.props.values.isChecked
    }

    const user = {
      userName: this.props.values.userName,
      password: this.props.values.password,
      portNumber: this.props.values.portNumber,
      dbms: this.props.values.dbms,
      database: this.props.values.database,
      serverName: this.props.values.serverName,
      query: this.props.values.query
    }
    // axios.post('http://localhost:8080/api/query/all', { user })

    //   axios.get('http://localhost:8080/api/query/all')
    //     .then(res => {
    //       console.log(res);
    //       console.log(res.data);
    //       this.setState({
    //         data: res.data,
    //         isActive: true
    //       })
    //       if (this.state.status == 2) {
    //         this.setState({
    //           isActive: false
    //         })
    //       };
    //     });
    // }


    axios.post('http://localhost:8082/anayltics-service/api/analytics/dbexcuteQuery', user
      // {
      //   "userName": "root",
      //   "password": "h.c.verma",
      //   "portNumber": "3306",
      //   "dbms": "MY_SQL",
      //   "query": "select * from client_db_1",
      //   "database": "client_db_1",
      //   "serverName": "localhost"
      // }
    ).then(res => {
      console.log(res);
      console.log(res.data);
      this.setState({
        data: res.data.payload,
        isActive: true,
        IsQueryInValid: false
      })
      if (this.state.status == 2) {
        this.setState({
          isActive: false
        })
      };
    }).catch((Error) => {
      this.setState({
        IsQueryInValid: true
      })
      console.log(Error);
    });
  };


  handleSave = event => {
    event.preventDefault();

    const query = {
      userId: this.props.values.userName,
      queryname: this.props.values.queryName,
      query: this.props.values.query
    }
    axios.post("http://localhost:8080/api/save", query)
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.setState({
          isActive: false,
          open: true
        })
      })
  }

  render() {
    const { status, data, open, IsQueryInValid } = this.state;
    const { values, handleChange, classes, prevStep, stateChange } = this.props;

    const queryData = {
      query: values.query,
      queryName: values.queryName,
      userName: values.userName
    }

    return (
      <div className="row">
        <div className="col-md-12">
          <h4 className="heading2">
            Execute Query
          </h4>
          <p className="para2">Please select the NewQuery or choose from Saved Query to Execute Query</p>
        </div>
        <MuiThemeProvider theme={theme}>
        <div className="col-md-12">
          <div>
            <div className={classes.root}>
              {/* <Paper elevation={3}> */}
              <div className="radio">
                <RadioGroup
                  row
                  aria-label="position"
                  name="position"
                  defaultValue="top"

                >
                  <FormControlLabel
                    value="NewQuery"
                    control={<Radio color="primary"
                      checked={status === 1}
                      onClick={(e) => this.radioHandler(1)} />}
                    label="New Query"
                  />
                </RadioGroup>
              </div>
              {/* </Paper> */}
              {/* <Paper elevation={3}> */}
              <div className="radio">
                <RadioGroup
                  row
                  aria-label="position"
                  name="position"
                  defaultValue="top"
                >
                  <FormControlLabel
                    value="SavedQuery"
                    control={<Radio color="primary"
                      checked={status === 2}
                      onClick={(e) => this.radioHandler(2)} />}
                    label="Saved Query"
                  />
                </RadioGroup>
              </div>
              {/* </Paper> */}
            </div>
            {(status === 1) ?
              <NewQuery IsQueryInValid={IsQueryInValid} queryData handleChange={handleChange} />
              : null}
            {(status === 2) ? <SaveQuery userName={values.userName} values={values} stateChange={stateChange} /> : null}

          </div>

          {this.state.isActive ?
            <div className={classes.jsontble}>
              <br />
              <JsonToTable json={data} />
              <br />
            </div>
            : null}

          <MuiThemeProvider>
            <>
              <form onSubmit={this.handleSubmit}>
                <div className={this.state.isActive ? classes.groupButton2 : classes.groupButton}>
                  <Button
                    onClick={this.back}
                    className={classes.back}
                  >Back to Database Configuration</Button>

                  {this.state.isActive ?
                    <Button
                      onClick={this.handleSave}
                      className={classes.save}
                    >Save Query</Button>
                    : null}
                  <Button
                    type="submit"
                    variant="contained"
                    className={classes.button}
                  >Connect to DB and Execute Query</Button>
                  <ToastContainer />
                </div>
              </form>
            </>
          </MuiThemeProvider>
        </div>
        </MuiThemeProvider>
        <div className={classes.rootsnack}>
          <Snackbar open={open} autoHideDuration={5000} onClose={this.handleClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
            <Alert onClose={this.handleClose}
              className={classes.snackbarStyleViaContentProps}>
              Query Saved Successfully
        </Alert>
          </Snackbar>
        </div>
      </div>
    );
  }
}

export default withStyles(useStyles)(FormPersonalDetails);