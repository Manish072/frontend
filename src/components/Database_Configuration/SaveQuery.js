import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
import { CsvToHtmlTable } from 'react-csv-to-table';
import Button from '@material-ui/core/Button';
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import CloseIcon from "@material-ui/icons/Close";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


const useStyles = theme => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: "2px",
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rootsnack: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(50)
    },
  },
  snackbarStyleViaContentProps: {
    backgroundColor: "#12805d"
  }



});
const styles = (theme) => ({
  root: {
    marginLeft: 7,
    padding: theme.spacing(2),
    color: "#032e58",
    fontFamily: "Effra",
    fontSize: "40px",
    fontWeight: 500,
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.4,
    letterSpacing: "normal",
    textAlign: "left",
    textTransform: "none"
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  },
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  title: {
    flex: '1 1 100%',
  },
}));
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    margin: 5,
    width: "450px",
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    fontSize: "20px",
    fontWeight: "400",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.25",
    letterSpacing: "normal",
    textAlign: "left",
    color: "#2e2e2e"
  }
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
    direction: "rtl",
  }
}))(MuiDialogActions);

const sampleData = `
No Data Available.
`;

class SavedQuery extends Component {

  constructor(props) {

    super(props);
    this.state = {

      data: [],
      selected: [],
      page: 0,
      rowsPerPage: 5,
      isLoaded: false,
      open: false,
      sopen: false,
    }; // 0: no show, 1: show yes, 2: show no. 

  }

  componentDidMount() {
    this.fectchData();
  }


  fectchData() {
    const id = this.props.userName;
    axios.get(`http://localhost:8080/api/query/id/${id}`)
      .then(res => {
        console.log("response data:" + res.data);
        this.setState({
          isLoaded: true,
          data: res.data
        })
      });
  }

  handleClickOpen = () => {
    this.setState({
      open: true
    })

  };

  handleClose = () => {
    this.setState({
      open: false
    })
  };

  handleSnackClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({
      sopen: false
    })
  };
  //  handleClick = () => {
  //   this.setState({
  //     sopen:true
  //   })
  // };
  // handleCloseSnackbar = (event, reason) => {
  //   if (reason === 'clickaway') {
  //     return;
  //   }

  //   this.setState({
  //     sopen:false
  //   })
  // };
  render() {

    const { classes, stateChange, values } = this.props;
    const { selected, page, rowsPerPage, data, isLoaded, open, sopen } = this.state;

    const handleClick = (event, name) => {
      // console.log('\n\n\n\n');
      // console.log(que);
      // console.log(values.query);  // Before
      // stateChange(que);
      // console.log(values.query); //  After
      // console.log();
      const selectedIndex = selected.indexOf(name);
      let newSelected = [];
      if (selectedIndex === -1) {
        newSelected = newSelected.concat(selected, name);
        // newSelected=[...selected, name];
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(selected.slice(1));
      } else if (selectedIndex === selected.length - 1) {
        newSelected = newSelected.concat(selected.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      }
      this.setState({
        selected: newSelected
      })

    };

    const handleChangePage = (event, newPage) => {
      this.setState({
        page: newPage
      })
    };

    const handleChangeRowsPerPage = (event) => {
      this.setState({
        rowsPerPage: parseInt(event.target.value, 10),
        page: 0
      })

    };
    const handleDeleteClick = (event) => {

      const queryList = {
        queryList: selected
      }

      axios.delete('http://localhost:8080/api/delete', { data: queryList })
        .then(res => {
          console.log(res);
          console.log(res.data);

          this.setState({
            selected: [],
            sopen: true,
          })
          this.fectchData();
        }).catch((Error) => {
          console.log(Error);
        });
    }
    // const isSelected = (name) => selected.length>0 && selected.some(data => data.indexOf(name) !== -1);
    const isSelected = (name) => selected.indexOf(name) !== -1;

    const EnhancedTableToolbar = (props) => {
      const classes = useToolbarStyles();
      const { numSelected } = props;

      return (
        <Toolbar
          className={clsx(classes.root, {
            [classes.highlight]: numSelected > 0,
          })}
        >
          {numSelected > 0 ? (
            <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
              {numSelected} selected
            </Typography>
          ) : (
              <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                Query Names
              </Typography>
            )}

          {numSelected > 0 ? (

            <Tooltip title="Delete" >
              <IconButton aria-label="delete" onClick={this.handleClickOpen} >
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          ) : null}
          {/* <Snackbar open={sopen} autoHideDuration={6000} onClose={this.handleCloseSnackbar}>
        <Alert onClose={this.handleCloseSnackbar} severity="success">
          This is a success message!
        </Alert>
      </Snackbar> */}
        </Toolbar>

      );
    };

    EnhancedTableToolbar.propTypes = {
      numSelected: PropTypes.number.isRequired,
    };
    function stableSort(array) {
      const stabilizedThis = array.map((el, index) => [el, index]);
      return stabilizedThis.map((el) => el[0]);
    }
    return (
      <div className={classes.root}>

        {(isLoaded && data) ?
          <Paper className={classes.paper}>
            <EnhancedTableToolbar numSelected={selected.length} />
            <TableContainer>
              <Table
                className={classes.table}
                aria-labelledby="tableTitle"
                aria-label="enhanced table"
              >

                <TableBody>
                  {
                    stableSort(this.state.data)
                      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      .map((row, index) => {
                        const isItemSelected = isSelected(row.queryname);
                        console.log("row" + row + "index" + index);
                        const labelId = `enhanced-table-checkbox-${index}`;
                        return (
                          <TableRow
                            hover
                            onClick={(event) => { handleClick(event, row.queryname, row.query); stateChange(row.query); }}
                            role="checkbox"
                            aria-checked={isItemSelected}
                            tabIndex={-1}
                            key={row.queryname}
                            selected={isItemSelected}
                          >
                            <TableCell padding="checkbox">
                              <Checkbox
                                checked={isItemSelected}
                                inputProps={{ 'aria-labelledby': labelId }}
                              />
                            </TableCell>
                            <TableCell component="th" id={labelId} scope="row" padding="none">
                              {row.queryname}
                            </TableCell>

                          </TableRow>
                        );
                      })}

                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={data.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Paper> :
          //   <Alert>
          //   <AlertTitle>No Data Available</AlertTitle>
          // </Alert>
          <div className="col-md-11">
            <CsvToHtmlTable
              className="table"
              data={this.state.csvData || sampleData}
              csvDelimiter=","
              tableClassName="table table-striped table-hover"
            />
          </div>
        }
        <Dialog
          onClose={this.handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
          PaperProps={{
            style: { borderRadius: 10 }
          }}
        >
          <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>
            Confirm
        </DialogTitle>
          <DialogContent>
            Are you sure you want to reset the database details?
        </DialogContent>
          <DialogActions>
            <Button

              onClick={this.handleClose}
              style={{ textTransform: "none", color: " #006496", fontWeight: "bold" }}
            >
              Not now
          </Button>
            <Button onClick={() => {
              handleDeleteClick();
              this.handleClose();
            }} variant="contained" style={{
              backgroundColor: "#c73232",
              color: "white",
              "&:hover": {
                backgroundColor: "#c73232"
              },
              width: "100px",
              textTransform: "none"
            }}>
              Yes
          </Button>
          </DialogActions>
        </Dialog>
        <div className={classes.root}>
          <Snackbar open={sopen} autoHideDuration={5000} onClose={this.handleSnackClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
            <Alert onClose={this.handleSnackClose}
              className={classes.snackbarStyleViaContentProps}>
              Query Deleted Successfully
        </Alert>
          </Snackbar>

        </div>
      </div>

    );
  }
}
export default withStyles(useStyles)(SavedQuery);