import React, { Component } from 'react'
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import FormHelperText from "@material-ui/core/FormHelperText";
import { red } from '@material-ui/core/colors';
import { FormPersonalDetails } from '../Database_Configuration/FormPersonalDetails';

const useStyles = theme => ({
  input: {
    height: 45,
    width: 250,

  },
  input2: {
    height: 95,
    width: "570%",

  },
  root: {
    '& > *': {
      margin: theme.spacing(1),
      marginLeft: -10
    },
  },
  formHelperText: {
    color: "red"
  }

});

export class NewQuery extends Component {
  state = {
    queryData: '',
  }
 
  render() {

    const { queryData, classes, handleChange, IsQueryInValid } = this.props;
    return (

      <div>
        <div className="col-md-12">
          <AppBar title="Enter Personal Details" />
          <form>
            <TextField
              variant="outlined"
              placeholder="Enter Query Name"
              className={classes.root}
              label="Query Name"
              InputProps={{
                className: classes.input
              }}
              onChange={handleChange('queryName')}
              defaultValue={queryData.queryName}
              margin="normal"
            />
            <br />
            <TextField
              variant="outlined"
              placeholder="Enter Query"
              label="Query"
              className={classes.root}
              InputProps={{
                className: classes.input2
              }}
              onChange={handleChange('query')}
              defaultValue={queryData.query}
              margin="normal"
              error={IsQueryInValid}
            />
            <FormHelperText className={classes.formHelperText} style={{ display: IsQueryInValid ? "block" : "none" }}>
              You have an error in your SQL syntax. Please try again
      </FormHelperText>
            <br />
          </form>

        </div>
      </div>
    );
  }
}
export default withStyles(useStyles)(NewQuery)