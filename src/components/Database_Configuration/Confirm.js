import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import { List, ListItem, ListItemText } from '@material-ui/core/';
import Button from '@material-ui/core/Button';
import axios from "axios";

export class Confirm extends Component {
  continue = e => {
    e.preventDefault();
    // PROCESS FORM //
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  handleSubmit = event => {
    event.preventDefault();
    if (this.props.values.isChecked && this.props.values.userName !== "") {
      localStorage.firstName = this.props.values.userName
      localStorage.checkbox = this.props.values.isChecked
  }

    const user = {
      userName: this.props.values.userName,
      password: this.props.values.password,
      portNumber: this.props.values.portNumber,
      dbms: this.props.values.dbms,
      database: this.props.values.database,
      serverName: this.props.values.serverName,
      query: this.props.values.query
    }
    axios.post('https://jsonplaceholder.typicode.com/users', { user })
      .then(res=>{
        console.log(res);
        console.log(res.data);
        })
      
  } 

  render() {
    const {
      values: { userName,
        password,
        portNumber, 
        dbms, 
        database,
        serverName,
        queryName,
        query}
    } = this.props;
    return (
      <MuiThemeProvider>
        <>
            <AppBar title="Confirm User Data" />
            <List>
              <ListItem>
                <ListItemText primary="User Name" secondary={userName} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Password" secondary={password} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Type Of Database" secondary={dbms} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Server Name" secondary={serverName} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Port Number" secondary={portNumber} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Database Name" secondary={database} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Query Name" secondary={queryName} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Query" secondary={query} />
              </ListItem>
            </List>
            <br />
            <form onSubmit={this.handleSubmit}>
            <Button
              color="secondary"
              variant="contained"
              onClick={this.back}
            >Back</Button>

            <Button
              type="submit"
              color="primary"
              variant="contained"
            >Confirm & Continue</Button>
          </form>
        </>
      </MuiThemeProvider>
    );
  }
}

export default Confirm;