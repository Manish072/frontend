import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import './FormUser.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import theme from './Theme';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";

const useStyles = theme => ({
  input: {
    height: 45,
    width:300,
    marginRight: theme.spacing(5),
    marginTop: theme.spacing(0.8),
    marginLeft: theme.spacing(0.8),
    color:"black"
  },
  button: {
    // boxShadow: '1000',
    textTransform: 'none',
    fontSize: 16,
    color: 'white',
    padding: '6px 12px',
    border: '1px solid',
    lineHeight: 1.95,
    backgroundColor: '#c73232',
    borderColor: '#c73232',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#c73232',
      borderColor: '#c73232',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#c73232',
      borderColor: '#c73232',
    },
    '&:focus': {
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      outline: "none"
    },
  },
  dialog:{
    width: "460px",
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    fontSize: "18px",
  fontWeight: "400",
  fontStretch: "normal",
  fontStyle: "normal",
  lineHeight: "1.25",
  letterSpacing: "normal",
  textAlign: "left",
  color: "#2e2e2e",
  textTransform: "none"
  },
  confmColo:{
    marginLeft:"-7px",
    color: "#032e58",
    fontFamily:"Effra",
    fontSize: "20px",
  fontWeight: 500,
  fontStretch: "normal",
  fontStyle: "normal",
  lineHeight: 1.4,
  letterSpacing: "normal",
  textAlign: "left",
  textTransform: "none"
  },
  notConf:{
    textTransform: "none",
    paddingLeft: "30px",
    outline: "none"
  },
  confir:{
    backgroundColor:"#c73232",
    color:"white",
    outline: "none",
    "&:hover": {
      backgroundColor:"#c73232"
    },
   width:"100px",
   textTransform: "none"
  },
  dialogAct:{
    padding: "17px",
    marginLeft: "-8px"
  },
  reset:  {
    marginLeft: theme.spacing(2),
    paddingRight:theme.spacing(5),
    //boxShadow: '1000',
    textTransform: 'none',
    fontSize: 16,
    color: '#004162',
    padding: '6px 12px',
    //border: '1px solid',
    lineHeight: 1.95,
    backgroundColor: 'white',
    boxShadow: 'none',
    //borderColor: 'black',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: 'white',
      borderColor: 'black',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: 'white',
      borderColor: 'black',
    },
    '&:focus': {
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      outline: "none"
    },
  },
  groupButton: {
    marginTop: theme.spacing(7),
    paddingLeft: theme.spacing(112)
  },
  checked: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(-5.5)
  },
  databaseStyle:{
    color:"black"
  },
  root: {
    "& .MuiFormLabel-root": {
      paddingLeft: theme.spacing(1)
    },
    height: 45,
    width:300,
    color:"black",
    // marginRight: theme.spacing(5),
    marginTop: theme.spacing(0.8),
    marginLeft: theme.spacing(0)
  },
  root1: {
    "& .MuiFormLabel-root": {
      paddingLeft: theme.spacing(-25)
    },
    paddingRight: theme.spacing(5)
  },
  root2:{
    marginLeft: "30px"
  },
  root3:{
    marginLeft: "1px"
  },
  // roup1: {
  //   marginLeft: theme.spacing(1),
  //   paddingTop: theme.spacing(10),
  // },
  // tex1: {
  //   padding: theme.spacing(0)
  // },
  // roup2: {
  //   marginLeft: theme.spacing(45),
  //   paddingTop: theme.spacing(10),
  // },
  // roup3: {
  //   marginLeft: theme.spacing(90),
  //   paddingTop: theme.spacing(10),
  // }
  formControl: {
    marginTop: theme.spacing(2),
    margin: theme.spacing(1),
    minWidth: 298,
    color: "black"
  },
  selectEmpty: {
    marginTop: theme.spacing(1),
  },
});

const styles = {
  row: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft:'10px',
    width: theme.spacing(200),
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
    color:"black"
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
}
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
    direction: "rtl"
  }
}))(MuiDialogActions);

export class FormUserDetails extends Component {
  constructor(props) {
    super(props)
    this.state = { open: false, mysql: "" } // 0: no show, 1: show yes, 2: show no.

  }
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };
  
   handleClickOpen = () => {
  
    this.setState({
      open:true
    })
   
  };
   handleClose = () => {
    this.setState({
      open:false
    })
  };

  handleMysqlChange = () => {
    this.setState({
      mysql: !this.state.mysql
    })
  };

  // handleChange (e) {
  //   console.log('handle change called')
  // }

  // handleClik() {
  //   this.setState( {disabled: !this.state.disabled} )
  // } 
    
  


  render() {
    const { values, handleChange, onChangeCheckbox, classes, validatorListener, disabled, handleReset} = this.props;
   const {open}=this.state;
    return (
      <div className="row">
        <div className="col-md-12">
            <h4 className="heading2">
             Database Details
            </h4>
            <p className="para2">Please furnish the details of the database schema that needs to be used to fetch data.</p>
        </div>
        <MuiThemeProvider theme={theme}>
        <>
            <AppBar title="Enter User Details" />
            {/* <ValidatorForm
              ref={(r) => { this.form = r; }}
              instantValidate
            > */}
            <div style={styles.row}>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="outlined-age-native-simple">
                  Database Type
                </InputLabel>
                <Select
                  native
                  // onChange={handleChange("dbms")}
                  onInput={handleChange("dbms")}
                  onChange={this.handleMysqlChange}
                  value={values.dbms}
                  margin="normal"
                  label="Database Type"
                  className={classes.root}
                  inputProps={{
                    name: classes.input,
                    id: "outlined-age-native-simple",
                  }}
                >
                  {/* <option aria-label="None" value="" /> */}
                  <option value={"MY_SQL"} className={classes.databaseStyle}>MY SQL</option>
                  <option value={"ORACLE"} className={classes.databaseStyle}>ORACLE</option>
                </Select>             
              </FormControl>
            <TextValidator
              validators={['required']}
              errorMessages={['required field']}
              validatorListener={validatorListener}
              className="field"
              variant="outlined"
              placeholder={(this.state.mysql)? "Enter SID" : "Enter Scheme/DB Name"}
              label="Schema/DB Name/SID"
              // disabled = {(this.state.mysql)? "disabled" : ""}
              className={classes.root2}
              InputProps={{
                className: classes.input
              }}
              onChange={handleChange('database')}
              value={values.database}
              margin="normal"
            /> 
             <TextValidator
              validators={['required']}
              errorMessages={['required field']}
              validatorListener={validatorListener}
              className="field"
              variant="outlined"
              placeholder="Enter Port Number Ex:1234"
              label="Port Number"
              className={classes.root3}
              InputProps={{
                className: classes.input
              }}
              onChange={handleChange('portNumber')}
              value={values.portNumber}
              margin="normal"
            />
            </div>
            <div style={styles.row}>
             <TextValidator
              validators={['required']}
              errorMessages={['required field']}
              className={classes.tex}
              validatorListener={validatorListener}
              variant="outlined"
              placeholder="Enter Server Name Ex: localhost"
              label="Server Name/Url"
              className={classes.root1}
              InputProps={{
                className: classes.input
              }}
              onChange={handleChange('serverName')}
              value={values.serverName}
              margin="normal"
            />
            <br/>
            </div>
            <div style={styles.row}>
            <TextValidator
              validators={['required']}
              errorMessages={['required field']}
              validatorListener={validatorListener}
              className={classes.tex}
              variant="outlined"
              placeholder="Enter UserName"
              label="Username"
              // className={classes.root}
              InputProps={{
                className: classes.input
              }}
              onChange={handleChange('userName')}
              value={values.userName}
              margin="normal"
            />
            <TextValidator
              validators={['required']}
              errorMessages={['required field']}
              validatorListener={validatorListener}
              className="field"
              variant="outlined"
              placeholder="Enter Password"
              label="Password"
              type="password"
              className={classes.root1}
              InputProps={{
                className: classes.input
              }}
              onChange={handleChange('password')}
              value={values.password}
              margin="normal"
            />
      <FormControlLabel
          control={
            <Checkbox
              checked={values.isChecked}
              onChange={onChangeCheckbox}
              name="checkedB"
              style ={{
                color: "#004162",
              }}
            />
          }
          className={classes.checked}
          label="Remember the Details"
      />
      </div>
            <br />
            <div className={classes.groupButton}>
            <Button
              variant="contained"
           //   onClick={handleReset}
             onClick={this.handleClickOpen}
           
              className={classes.reset}
            >Reset</Button>
            <Button
              variant="contained"
              onClick={this.continue}
              className={classes.button}
              disabled={disabled}
            >Proceed to Execute Query</Button>
             <Dialog
        onClose={this.handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle disableTypography id="customized-dialog-title" onClose={this.handleClose} className={classes.confmColo}>
          Confirm
        </DialogTitle>
        <DialogContent className={classes.dialog}>
          Are you sure you want to reset the database details?
        </DialogContent>
        <DialogActions className={classes.dialogAct}>
          <Button autoFocus onClick={this.handleClose} color="primary" className={classes.notConf}>
            Not now
          </Button>
          <Button autoFocus   onClick={() => {
              handleReset();
             this.handleClose();
            }} className={classes.confir} variant="contained" >
            Yes
          </Button>
        </DialogActions>
      </Dialog>

            </div>
            {/* </ValidatorForm> */}
        </>
      </MuiThemeProvider>
      </div>
    );
  }
}

export default withStyles(useStyles)(FormUserDetails);