import React, { Component } from 'react';
import FormUserDetails from './FormUserDetails';
import FormPersonalDetails from './FormPersonalDetails';
import Confirm from './Confirm';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import './FormUser.css';

export class UserForm extends Component {
  state = {
    step: 1,
    userName: '',
    password: '',
    portNumber: '',
    dbms: '',
    database: '',
    serverName: '',
    queryName: '',
    query: '',
    isChecked: false,
    disabled: false,

  };

  // Proceed to next step
  nextStep = () => {
    this.form.isFormValid(false).then((isValid) => {
      if (isValid) {
        const { step } = this.state;
        this.setState({
          step: step + 1
        });
      }
    });
  };
  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1
    });
  };

  stateChange = (q) => {
    this.setState({
      query: q
    })
  }

  // Handle fields change
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  };

  onChangeCheckbox = event => {
    this.setState({
      isChecked: event.target.checked
    })
  }

  validatorListener = (result) => {
    this.setState({ disabled: !result });
  }

  componentDidMount() {
    if (localStorage.checkbox && localStorage.firstName !== "") {
      this.setState({
        isChecked: true,
        firstName: localStorage.firstName
      })
    }
  }

  handleReset = () => {
    this.setState({
      userName: '',
      password: '',
      portNumber: '',
      dbms: '',
      database: '',
      serverName: '',

    })
  }

  render() {
    const { step } = this.state;
    const { userName,
      password,
      portNumber,
      dbms,
      database,
      serverName,
      queryName,
      query,
      isChecked,
      disabled } = this.state;

    const values = {
      userName,
      password,
      portNumber,
      dbms,
      database,
      serverName,
      queryName,
      query,
      isChecked
    };

    switch (step) {
      case 1:
        return (
          <ValidatorForm
            ref={(r) => { this.form = r; }}
            instantValidate
            className="for"
          >
            <FormUserDetails
              nextStep={this.nextStep}
              handleChange={this.handleChange}
              values={values}
              onChangeCheckbox={this.onChangeCheckbox}
              validatorListener={this.validatorListener}
              disabled={disabled}
              handleReset={this.handleReset}

            />
          </ValidatorForm>
        );
      case 2:
        return (
          <FormPersonalDetails
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            stateChange={this.stateChange}
            values={values}
          />
        );
      case 3:
        return (
          <Confirm
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            values={values}
          />
        );
      default:
        (console.log('This is a multi-step form built with React.'))
    }
  }
}

export default UserForm;