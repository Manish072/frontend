import { createMuiTheme} from '@material-ui/core/styles';

const Theme = createMuiTheme({
    palette: {
        primary: {
            main: '#004162'
        }
    }
})

export default Theme