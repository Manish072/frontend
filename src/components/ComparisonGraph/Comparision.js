/* App.js */
import React, { Component } from "react";
import Dialog from '@material-ui/core/Dialog';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Deployment from "../../components/Deployment/deployment"
import CanvasJSReact from '../../assets/canvasGraph/canvasjs.stock.react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import theme from '../../components/Database_Configuration/Theme';
import Tooltip from '@material-ui/core/Tooltip';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import KPI from '../KPIGraph/Kpi';
import { deployment } from "actions";
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSStockChart = CanvasJSReact.CanvasJSStockChart;

 
const useStyles = theme => ({
root: {
  marginTop: theme.spacing(4),
    paddingLeft: theme.spacing(104)
},
selectedModel: {
  color: 'Red'
},
  button: {
    boxShadow: '1000',
    textTransform: 'none',
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(18),
    fontSize: 16,
    color: 'white',
    padding: '6px 12px',
    border: '1px solid',
    lineHeight: 1.95,
    backgroundColor: '#c73232',
    borderColor: 'none',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#c73232',
      borderColor: '#c73232',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#c73232',
      borderColor: '#c73232',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      outline: "none"
    },
  },
  buttonEnabled: {
    boxShadow: '1000',
    textTransform: 'none',
    fontSize: 16,
    color: 'white',
    padding: '6px 12px',
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(18),
    border: '1px solid',
    lineHeight: 1.95,
    backgroundColor: '#c73232',
    borderColor: '#c73232',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#c73232',
      borderColor: '#c73232',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#c73232',
      borderColor: '#c73232',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      outline: "none"
    },
  },
  // selectTooltip:{
  //   fontSize:"15px",
  //  padding: '4px 195px 4px 4px',
  
  //  },
  ModelGraph:{
    width: "95%",
    height: "97%",
    marginLeft: "30px"
  },
  dialogAct:{
    padding: "17px",
      marginLeft: "-8px"
    },
    dialogContent1:{
      width:"100%",
      marginBottom: theme.spacing(-4),
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      fontSize: "20px",
    fontWeight: "400",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.25",
    letterSpacing: "normal",
    textAlign: "left",
    color: "#032e58",
    textTransform: "none"
    },
    dialogContent2:{
      width:"100%",
      height: "10%",
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      fontSize: "18px",
    fontWeight: "400",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.25",
    letterSpacing: "normal",
    textAlign: "left",
    textTransform: "none"
    },
  dialog:{
    width: theme.spacing(40),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    fontSize: "20px",
  fontWeight: "400",
  fontStretch: "normal",
  fontStyle: "normal",
  lineHeight: "1.25",
  letterSpacing: "normal",
  textAlign: "left",
  color: "#2e2e2e",
  textTransform: "none"
  },
  confmColo:{
    marginLeft:"-7px",
    color: "#032e58",
    fontFamily:"Effra",
    fontSize: "20px",
  fontWeight: 500,
  fontStretch: "normal",
  fontStyle: "normal",
  lineHeight: 1.4,
  letterSpacing: "normal",
  textAlign: "left",
  textTransform: "none"
  },
  notConf:{
    textTransform: "none",
    paddingLeft: "30px",
    color: "#032e58"
  },
  confir:{
    backgroundColor:"#c73232",
    color:"white",
    "&:hover": {
      backgroundColor:"#c73232"
    },
   width:"100px",
   textTransform: "none"
  },
});

const styles = {
  row: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft:'16px'
  },
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
}

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
    direction: "rtl"
  }
}))(MuiDialogActions);

class Comparision extends Component {

  handleClickOpen = () => {

    if((this.props.workDays[0]!=='Model1' && this.props.workDays[1]!=='Model2' && this.props.workDays[2]!==3)
     && (this.props.workDays[0]!=='Model3' && this.props.workDays[1]!=='Model2' && this.props.workDays[2]!=='Model1')
     && (this.props.workDays[0]!=='Model2' && this.props.workDays[1]!=='Model3')) {
      this.setState({
        selectModel: true,
        open:false
      })
    }
    else{
      this.setState({
        open:true
      })
    }
   
  };
   handleClose = () => {
    this.setState({
      open:false,
      selectModel: false,
    })


  };

  // handleSelecModel = () => {
  //   if(this.props.workDays[0]!=='Model1' && this.props.workDays[1]!=='Model2') {
  //     this.setState({
  //       selectModel: false
  //     })
  //   }
  // };

  constructor(props) {
    super(props);
    this.state = { dataPoints: [], dataPoints1: [] , dataPoints2: [], dataPoints3: [],
      dataPoints4: [] , dataPoints5: [], dataPoints6: [] , isLoaded: false ,
    dataCheckBox: '', isLoadedActualModel: false, open: false, selectModel: false
  };
  }
 
  componentDidMount() {
    //Reference: https://reactjs.org/docs/faq-ajax.html#example-using-ajax-results-to-set-local-state
    fetch("https://api.mocki.io/v1/64f522e0")
      .then(res => res.json())
      .then(
        (data) => {
          var dps=[];
          var dps1=[];
          var dps2=[];
          var dps3= [];
          var dps4= [];
          var dps5= [];
          var dps6= [];
          for (var i = 0; i < data.length; i++) {
            if(data[i].Metric=='Actual'){
            dps.push({
              x: new Date(data[i].operating_date),
              y: Number(data[i].standBy)
            });
          }
          else if(data[i].Metric=='LSTM'){
            dps1.push({
              x: new Date(data[i].operating_date),
              y: Number(data[i].predicted_count)
            });
            dps4.push({
              x: new Date(data[i].operating_date),
              y: Number(data[i].MAPE_error)
            });
          }
          else if(data[i].Metric=='CNN'){
            dps2.push({
              x: new Date(data[i].operating_date),
              y: Number(data[i].predicted_count)
            });
            dps5.push({
              x: new Date(data[i].operating_date),
              y: Number(data[i].MAPE_error)
            });
          }
          else if(data[i].Metric=='GRU'){
            dps3.push({
              x: new Date(data[i].operating_date),
              y: Number(data[i].predicted_count)
            });
            dps6.push({
              x: new Date(data[i].operating_date),
              y: Number(data[i].MAPE_error)
            });
          }
          }
          this.setState({
            isLoadedActualModel: true,
            dataPoints: dps,
            dataPoints1: dps1,
            dataPoints2:dps2,
            dataPoints3:dps3,
            dataPoints4: dps4,
            dataPoints5:dps5,
            dataPoints6:dps6
          });
        }
      )

      // fetch("https://canvasjs.com/data/docs/btcusd2018.json")
      // .then(res => res.json())
      // .then(
      //   (data1) => {
      //     var dps1 = [];
      //     console.log('heelelelelellelelel');
      //     console.log(data1);
      //     for (var j = 0; j < data1.length; j++) {
      //       dps1.push({
      //         x: new Date(data1[j].date),
      //         y: Number(data1[j].close)
      //       });
      //     }
      //     this.setState({
      //       isLoaded: true,
      //       dataPoints1: dps1
      //     });
      //   }
      // )

      // fetch("https://api.mocki.io/v1/95e6f5c2")
      // .then(res => res.json())
      // .then(
      //   (data2) => {
      //     var dps2 = [];
      //     for (var k = 0; k < data2.length; k++) {
      //       dps2.push({
      //         x: new Date(data2[k].date),
      //         y: Number(data2[k].close)
      //       });
      //     }
      //     this.setState({
      //       isLoaded: true,
      //       dataPoints2: dps2
      //     });
      //   }
      // )
  }
 
  render() {
    const {classes, disabled} = this.props;
    let graph;
    const options = {
      title:{
        text:""
      },
      exportEnabled: true,
      theme: "light2",
      subtitles: [{
        text: ""
      }],
      charts: [{
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "MMM DD YYYY"
          }
        },
        axisY: {
          title: "Stand By Count",
          suffix: "",
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "#"
          }
        },
        toolTip: {
          shared: true
        },
        data: [
          {
                name: "Actual Standby Count",
                type: "spline",
                showInLegend: true,
                legendText: "Actual Standby Count",
                color: "#1f78b4",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints
          },
        ],
      }],
      navigator: {
        slider: {
          minimum: new Date("01-02-2019"),
          maximum: new Date("01-12-2021")
        }
      }
    };

    const optionsActualModel1 = {
      title:{
        text:""
      },
      exportEnabled: true,
      theme: "light2",
      subtitles: [{
        text: ""
      }],
      charts: [{
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "MMM DD YYYY"
          }
        },
        axisY: {
          title: "Stand By Count",
          suffix: "",
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "#"
          }
        },
        toolTip: {
          shared: true
        },
        data: [
          {
                name: "Actual Standby Count",
                type: "spline",
                color: "#1f78b4",
                showInLegend: true,
                legendText: "Actual Standby Count",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints
          },
          {
                name: "LSTM Standby Count",
                type: "spline",
                color: "#f3b813",
                showInLegend: true,
                legendText: "LSTM",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints1
          },
        ],
      }],
      navigator: {
        slider: {
          minimum: new Date("01-02-2019"),
          maximum: new Date("01-12-2021")
        }
      }
    };

    const optionsActualModel1Model2 = {
      title:{
        text:""
      },
      exportEnabled: true,
      theme: "light2",
      subtitles: [{
        text: ""
      }],
      charts: [{
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "MMM DD YYYY"
          }
        },
        axisY: {
          title: "Stand By Count",
          suffix: "",
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "#"
          }
        },
        toolTip: {
          shared: true
        },
        data: [
          {
                name: "Actual Standby Count",
                type: "spline",
                color: "#1f78b4",
                showInLegend: true,
                legendText: "Actual Standby Count",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints
          },
          {
                name: "LSTM Predicted Count",
                type: "spline",
                color: "#f3b813",
                showInLegend: true,
                legendText: "LSTM",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints1
          },
          {
            name: "CNN Predicted Count",
            type: "spline",
            color: "#80271b",
            showInLegend: true,
            legendText: "CNN",
            yValueFormatString: "#",
            xValueFormatString: "MMM DD YYYY",
            dataPoints : this.state.dataPoints2
      },
        ],
      }],
      navigator: {
        slider: {
          minimum: new Date("01-02-2019"),
          maximum: new Date("01-12-2021")
        }
      }
    };

    const optionsActualModel2 = {
      title:{
        text:""
      },
      exportEnabled: true,
      theme: "light2",
      subtitles: [{
        text: ""
      }],
      charts: [{
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "MMM DD YYYY"
          }
        },
        axisY: {
          title: "Stand By Count",
          suffix: "",
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "#"
          }
        },
        toolTip: {
          shared: true
        },
        data: [
          {
                name: "Actual Standby Count",
                type: "spline",
                color: "#1f78b4",
                showInLegend: true,
                legendText: "Actual Standby Count",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints
          },
          {
            name: "CNN Predicted Count",
            type: "spline",
            color: "#80271b",
            showInLegend: true,
            legendText: "CNN",
            yValueFormatString: "#",
            xValueFormatString: "MMM DD YYYY",
            dataPoints : this.state.dataPoints2
      },
        ],
      }],
      navigator: {
        slider: {
          minimum: new Date("01-02-2019"),
          maximum: new Date("01-12-2021")
        }
      }
    };

    const optionsActualModel1Model2Model3 = {
      title:{
        text:""
      },
      exportEnabled: true,
      theme: "light2",
      subtitles: [{
        text: ""
      }],
      charts: [{
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "MMM DD YYYY"
          }
        },
        axisY: {
          title: "Stand By Count",
          suffix: "",
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "#"
          }
        },
        toolTip: {
          shared: true
        },
        data: [
          {
                name: "Actual Standby Count",
                type: "spline",
                color: "#1f78b4",
                showInLegend: true,
                legendText: "Actual Standby Count",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints
          },
          {
                name: "LSTM Predicted Count",
                type: "spline",
                color: "#f3b813",
                showInLegend: true,
                legendText: "LSTM",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints1
          },
          {
            name: "CNN Predicted Count",
            type: "spline",
            color: "#80271b",
            showInLegend: true,
            legendText: "CNN",
            yValueFormatString: "#",
            xValueFormatString: "MMM DD YYYY",
            dataPoints : this.state.dataPoints2
      },
      {
        name: "GRU Predicted Count",
        type: "spline",
        color: "#28a647",
        showInLegend: true,
        legendText: "GRU",
        yValueFormatString: "#",
        xValueFormatString: "MMM DD YYYY",
        dataPoints : this.state.dataPoints3
  },

        ],
      }],
      navigator: {
        slider: {
          minimum: new Date("01-02-2019"),
          maximum: new Date("01-12-2021")
        }
      }
    };

    const optionsActualModel1Model3 = {
      title:{
        text:""
      },
      exportEnabled: true,
      theme: "light2",
      subtitles: [{
        text: ""
      }],
      charts: [{
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "MMM DD YYYY"
          }
        },
        axisY: {
          title: "Stand By Count",
          suffix: "",
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "#"
          }
        },
        toolTip: {
          shared: true
        },
        data: [
          {
                name: "Actual Standby Count",
                type: "spline",
                color: "#1f78b4",
                showInLegend: true,
                legendText: "Actual Standby Count",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints
          },
          {
                name: "LSTM Predicted Count",
                type: "spline",
                color: "#f3b813",
                showInLegend: true,
                legendText: "LSTM",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints1
          },
      {
        name: "GRU Predicted Count",
        type: "spline",
        color: "#28a647",
        showInLegend: true,
        legendText: "GRU",
        yValueFormatString: "#",
        xValueFormatString: "MMM DD YYYY",
        dataPoints : this.state.dataPoints3
  },

        ],
      }],
      navigator: {
        slider: {
          minimum: new Date("01-02-2019"),
          maximum: new Date("01-12-2021")
        }
      }
    };

    const optionsActualModel2Model3 = {
      title:{
        text:""
      },
      exportEnabled: true,
      theme: "light2",
      subtitles: [{
        text: ""
      }],
      charts: [{
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "MMM DD YYYY"
          }
        },
        axisY: {
          title: "Stand By Count",
          suffix: "",
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "#"
          }
        },
        toolTip: {
          shared: true
        },
        data: [
          {
                name: "Actual Standby Count",
                type: "spline",
                color: "#1f78b4",
                showInLegend: true,
                legendText: "Actual Standby Count",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints
          },
          {
            name: "CNN Predicted Count",
            type: "spline",
            color: "#80271b",
            showInLegend: true,
            legendText: "CNN",
            yValueFormatString: "#",
            xValueFormatString: "MMM DD YYYY",
            dataPoints : this.state.dataPoints2
      },
      {
        name: "GRU Predicted Count",
        type: "spline",
        color: "#28a647",
        showInLegend: true,
        legendText: "GRU",
        yValueFormatString: "#",
        xValueFormatString: "MMM DD YYYY",
        dataPoints : this.state.dataPoints3
  },

        ],
      }],
      navigator: {
        slider: {
          minimum: new Date("01-02-2019"),
          maximum: new Date("01-12-2021")
        }
      }
    };

    const optionsActualModel3 = {
      title:{
        text:""
      },
      exportEnabled: true,
      theme: "light2",
      subtitles: [{
        text: ""
      }],
      charts: [{
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "MMM DD YYYY"
          }
        },
        axisY: {
          title: "Stand By Count",
          suffix: "",
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "#"
          }
        },
        toolTip: {
          shared: true
        },
        data: [
          {
                name: "Actual Standby Count",
                type: "spline",
                color: "#1f78b4",
                showInLegend: true,
                legendText: "Actual Standby Count",
                yValueFormatString: "#",
                xValueFormatString: "MMM DD YYYY",
                dataPoints : this.state.dataPoints
          },
      {
        name: "GRU Predicted Count",
        type: "spline",
        color: "#28a647",
        showInLegend: true,
        legendText: "GRU",
        yValueFormatString: "#",
        xValueFormatString: "MMM DD YYYY",
        dataPoints : this.state.dataPoints3
  },

        ],
      }],
      navigator: {
        slider: {
          minimum: new Date("01-02-2019"),
          maximum: new Date("01-12-2021")
        }
      }
    };

    const options_MAPE_Error_Actualodel1Model2Model3 = {
      title:{
        text:""
      },
      exportEnabled: true,
      theme: "light2",
      subtitles: [{
        text: ""
      }],
      charts: [{
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "MMM DD YYYY"
          }
        },
        axisY: {
          title: "MAPE Error",
          suffix: "%",
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "# '%'"
          }
        },
        toolTip: {
          shared: true
        },
        data: [
          {
            name: "LSTM MAPE Error",
            type: "spline",
            suffix: "%",
            color: "#f3b813",
            showInLegend: true,
            legendText: "LSTM",
            yValueFormatString: "# '%'",
            xValueFormatString: "MMM DD YYYY",
            dataPoints : this.state.dataPoints4
      },
          {
            name: "CNN MAPE Error",
            type: "spline",
            color: "#80271b",
            suffix: "%",
            showInLegend: true,
            legendText: "CNN",
            yValueFormatString: "# '%'",
            xValueFormatString: "MMM DD YYYY",
            dataPoints : this.state.dataPoints5
      },
      {
        name: "GRU MAPE Error",
        type: "spline",
        suffix: "%",
        color: "#28a647",
        showInLegend: true,
        legendText: "GRU",
        yValueFormatString: "# '%'",
        xValueFormatString: "MMM DD YYYY",
        dataPoints : this.state.dataPoints6
  },

        ],
      }],
      navigator: {
        slider: {
          minimum: new Date("01-02-2019"),
          maximum: new Date("01-12-2021")
        }
      }
    };

    const containerProps = {
      width: "100%",
      height: "450px",
      margin: "auto"
    };

    if ((this.props.workDays[0]=='Model1') && (this.props.workDays[1]==undefined) && (this.props.toggleStatus!=='On')){
      console.log('hello actual model1')
      graph = <CanvasJSStockChart containerProps={containerProps} options = {optionsActualModel1}/>;
    }
    else if ((((this.props.workDays[0]=='Model1') && (this.props.workDays[1]== 'Model2') && (this.props.toggleStatus!=='On')) ||
     ((this.props.workDays[0]=="Model2") && (this.props.workDays[1]=="Model1") && (this.props.toggleStatus!=='On')))
     &&(this.props.workDays[2]!=='Model3')){
      {console.log('hello combined')}
      graph = <CanvasJSStockChart containerProps={containerProps} options = {optionsActualModel1Model2}/>;
    }
    else if ((this.props.workDays[0]=='Model2') && (this.props.workDays[1]==undefined) && (this.props.toggleStatus!=='On')){
      {console.log('hello actual model 2')}
      graph = <CanvasJSStockChart containerProps={containerProps} options = {optionsActualModel2}/>;
    }
    else if ((this.props.model3== 6)&& (this.props.model2!=4) && 
    (this.props.model1!==2) && (this.props.toggleStatus!=='On')){
      console.log('hello actual model 3')
      graph = <CanvasJSStockChart containerProps={containerProps} options = {optionsActualModel3}/>;
    }
    else if((this.props.model1==2)&& (this.props.model2==4) && 
    (this.props.model3==6) && (this.props.toggleStatus!=='On')&& (this.props.parameters!=='MAPE Error')){
      console.log('hello actual model 1 2 3')
      graph = <CanvasJSStockChart containerProps={containerProps} options = {optionsActualModel1Model2Model3}/>;
    }
    else if((this.props.model2==4)&& (this.props.model3==6) && (this.props.parameters!=='MAPE Error') && (this.props.toggleStatus!=='On')){
      console.log('hello actual model 2 3')
      graph = <CanvasJSStockChart containerProps={containerProps} options = {optionsActualModel2Model3}/>;
    }
    else if(((this.props.model1==2)&& (this.props.model3==6)&& (this.props.parameters!=='MAPE Error')&& (this.props.toggleStatus!=='On'))){
      console.log('hello actual model 1 3')
      graph = <CanvasJSStockChart containerProps={containerProps} options = {optionsActualModel1Model3}/>;
    }
    else if((this.props.parameters=='MAPE Error')&& (this.props.model1==2)&& (this.props.model2==4) && 
    (this.props.model3==6) && (this.props.toggleStatus!=='On')){
      console.log('hello inside MAPE Error')
      graph = <CanvasJSStockChart containerProps={containerProps} options = {options_MAPE_Error_Actualodel1Model2Model3}/>;
    }
    else if ((this.props.toggleStatus=='On')){
      console.log('inside kpi');
      graph = <KPI/>
    }
    else {
      {console.log('hello actual')}
      graph = <CanvasJSStockChart containerProps={containerProps} options = {options}/>;
    }
    return (
      <div className={classes.ModelGraph}> 
        <div>{
              console.log(this.state.dataPoints4),
              console.log(this.state.dataPoints5),
              console.log(this.state.dataPoints6),
              console.log(this.state.dataPoints3),
              console.log(this.state.dataPoints2),
              console.log(this.state.dataPoints1),
              console.log(this.state.dataPoints),
              console.log(this.props.workDays),
              console.log(this.props.status),
              console.log(this.props.statusActual),
              console.log(this.props.workDays[1]),
              console.log(this.props.workDays[0])}
              {graph}
            
        </div>
        {(this.props.CustomerStatus!==4)?
        <div className={classes.root}>
          {(this.props.workDays[0]!=='Model1' && this.props.workDays[1]!=='Model2' && this.props.workDays[2]!=='Model3')
     && (this.props.workDays[0]!=='Model3' && this.props.workDays[1]!=='Model2' && this.props.workDays[1]!=='Model1') 
     && (this.props.workDays[0]!=='Model2' && this.props.workDays[1]!=='Model3')?
     <Tooltip title="Select at Least one Model " placement="top" className={classes.selectTooltip} style={{paddingRight: theme.spacing(18)}} arrow>
            <span>
              <Button
              variant="contained"
              disabled
              onClick={this.handleClickOpen}
              className={classes.button}
            >Deploy to Production</Button>
            </span>
          </Tooltip>:
            <Button
              variant="contained"
              onClick={this.handleClickOpen}
              className={classes.buttonEnabled}
            >Deploy to Production</Button>
          }
            <Dialog
        onClose={this.handleClose}
        aria-labelledby="customized-dialog-title"
        open={this.state.open}
      >
        <DialogTitle disableTypography id="customized-dialog-title" onClose={this.handleClose} className={classes.confmColo}>
          Confirm
        </DialogTitle>
        <DialogContent className={classes.dialogContent1}>
         {(this.props.workDays[0]=='Model1')&&(this.props.workDays[1]!=='Model2')&&(this.props.model3!==6)? <p><b>LSTM</b> is selected</p>:null }
        {(this.props.workDays[0]=='Model2')&&(this.props.workDays[1]!=='Model1')&&(this.props.model3!==6)? <p><b>CNN</b> is selected</p>:null }
        {((this.props.workDays[0]=='Model1') && (this.props.workDays[1]=='Model2')&&(this.props.model3!==6))||
        ((this.props.workDays[0]=='Model2') && (this.props.workDays[1]=='Model1')&&(this.props.model3!==6))
        ?<p><b>LSTM</b> and <b>CNN</b> are selected</p>:null}
        {(this.props.model3==6 && this.props.model2!==4 && this.props.model1!==2)?<p><b>GRU</b> is selected</p>:null}
        {(this.props.model3==6 && this.props.model2==4 && this.props.model1!==2)?<p><b>CNN</b> <b>and GRU</b> are selected</p>:null}
        {(this.props.model3==6 && this.props.model1==2 && this.props.model2!==4)?<p><b>LSTM</b> <b>and GRU</b> are selected</p>:null}
        {(this.props.model3==6)&&(this.props.model2==4)&&(this.props.model1==2)?<p><b>LSTM</b>, <b>CNN</b> and <b>GRU</b> are selected</p>:null}
        </DialogContent>
        <DialogContent className={classes.dialogContent2}>
        Are you sure do you want to deploy to production?
        </DialogContent>
        <DialogActions className={classes.dialogAct}>
        <Button autoFocus onClick={this.handleClose} color="primary" className={classes.notConf}>
            Not now
          </Button>
          {!(this.state.selectModel)?
        <Button autoFocus 
          onClick={() => {
             this.handleClose();
            }} className={classes.confir} variant="contained" >
            Yes
        </Button>:
  <Button autoFocus
  disabled 
  onClick={() => {
     this.handleClose();
    }} className={classes.confir} variant="contained" >
    Yes
</Button>
  }

        </DialogActions>
      </Dialog>
    {/* <button>Save settings</button> */}
    {/* <Display data={this.state.workDays}/> */}
  </div>:null
  }
  </div>
    );
  }
}
 
export default withStyles(useStyles)(Comparision);