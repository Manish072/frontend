import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(1),
      width: theme.spacing(20),
      height: theme.spacing(16)
    }
  },
  boxstyle: {
     margin: 'auto',
    width: theme.spacing(43),
    height: theme.spacing(23),
    alignItems: "center",
    justify: "center",
    borderRadius:10,
  },
  txtStyle:
    { fontWeight:"bold" ,fontSize:"18px",fontFamily:"Effra", margin:"30px 40px" },
  button: {
    // marginLeft: theme.spacing(109),
    // marginTop: theme.spacing(5),
   marginLeft:theme.spacing(8),
    boxShadow: "1000",
    textTransform: "none",
    fontSize: 16,
    color: "white",
    padding:"15px 30px",
    border: "1px solid",
    lineHeight: 1.95,
    backgroundColor: "#c73232",
    borderColor: "#c73232",
    fontFamily: ["Effra",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    "&:hover": {
      backgroundColor: "#c73232",
      borderColor: "#c73232",
      boxShadow: "none",
      
    },
    "&:active": {
      boxShadow: "none",
      backgroundColor: "#c73232",
      borderColor: "#c73232"
    },
    "&:focus": {
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.5)",
      outline: "none"
    }
  },
  linkStyle:{
      margin:'2% 43%',
      textDecoration:"underline",
      fontFamily:'Effra',
      fontSize:"16px",
      fontWeight:"bold"
  
  }
}));

export default function Deployment(props) {
  const classes = useStyles();
  return (
      <div>
    <div className={classes.root}>
      {/* <div className="col-md-12">
        <h4 className="heading2">Model Deployment</h4>
      </div> */}
      <Paper elevation={3} className={classes.boxstyle}>
        <div>
          <p className={classes.txtStyle}>
            {props.workDays} is selected for Deployment
          </p>
          <Button type="submit" variant="contained" className={classes.button}>
            Deploy to Production
          </Button>
        </div>
      </Paper>
    </div>
</div>
  );
}
