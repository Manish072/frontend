/* App.js */
import React, { Component } from "react";
import CanvasJSReact from '../../assets/canvasGraph/canvasjs.stock.react';
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSStockChart = CanvasJSReact.CanvasJSStockChart;
 
 
class Model extends Component {
  constructor(props) {
    super(props);
    this.state = { dataPoints: [], isLoaded: false };
  }
 
  componentDidMount() {
    //Reference: https://reactjs.org/docs/faq-ajax.html#example-using-ajax-results-to-set-local-state
    fetch("https://api.mocki.io/v1/f2fadb9f")
      .then(res => res.json())
      .then(
        (data) => {
          var dps = [];
          console.log('heheeheheheheheheh')
          console.log(data)
          for (var i = 0; i < data.length; i++) {
            dps.push({
              x: new Date(data[i].operating_date),
              y: Number(data[i].reservation_count)
            });
          }
          this.setState({
            isLoaded: true,
            dataPoints: dps
          });
        }
      )
  }
 
  render() {
    const options = {
      title:{
        text:""
      },
      exportEnabled: true,
      theme: "light2",
      subtitles: [{
        text: "Error rates, Efficiency etc"
      }],
      charts: [{
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "MMM DD YYYY"
          }
        },
        axisY: {
          title: "Conversion Rates/ Efficiency",
          suffix: "",
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
            valueFormatString: "##"
          }
        },
        toolTip: {
          shared: true
        },
        data: [{
          name: "Error Rates(%)",
          type: "spline",
          color: "#1f78b4",
          yValueFormatString: "####",
          xValueFormatString: "MMM DD YYYY",
          dataPoints : this.state.dataPoints
        }]
      }],
      navigator: {
        slider: {
          minimum: new Date("03-07-2019"),
          maximum: new Date("04-07-2021")
        }
      }
    };
    const containerProps = {
      width: "100%",
      height: "450px",
      margin: "auto"
    };
    return (
      <div> 
        <div>
          {
            console.log(this.state.dataPoints),
            // Reference: https://reactjs.org/docs/conditional-rendering.html#inline-if-with-logical--operator
            this.state.isLoaded && 
            <CanvasJSStockChart containerProps={containerProps} options = {options}
              /* onRef = {ref => this.chart = ref} */
            />
          }
        </div>
      </div>
    );
  }
}
 
export default Model;