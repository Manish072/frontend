import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Paper from '@material-ui/core/Paper';
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from '@material-ui/core/FormControl';
import Switch from "@material-ui/core/Switch";
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import PredictionGraph from 'views/Predict/PredictionGraph';
import './Predictcsv.css'
import theme from './Theme';
import { FormGroup, Link } from '@material-ui/core';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormHelperText from '@material-ui/core/FormHelperText';
import Tooltip from '@material-ui/core/Tooltip';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { useState } from 'react'
import { enGB } from 'date-fns/locale'
import { DateRangePicker, START_DATE, END_DATE } from 'react-nice-dates'
import 'react-nice-dates/build/style.css'
import { DateRange } from 'react-date-range';
import axios from "axios";
import ProgressBar from 'react-bootstrap/ProgressBar';
import CheckBoxSelection from "../../components/Comparisions/CheckBoxSelection";
import Dialog from '@material-ui/core/Dialog';
import CanvasJSReact from '../../assets/canvasGraph/canvasjs.stock.react';
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { JsonToTable } from 'react-json-to-table';
import { Collapse } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
// import KPI from '../KPIGraph/Kpi';
// import { deployment } from "actions";
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSStockChart = CanvasJSReact.CanvasJSStockChart;



// const [state, setState] = useState([
//   {
//     startDate: new Date(),
//     endDate: null,
//     key: 'selection'
//   }
// ]);






const useStyles = theme => ({
  root: {
    "& .MuiFormLabel-root": {
      paddingLeft: theme.spacing(1)
    },
    height: 45,
    width: 250,
    color: "black",
    // marginRight: theme.spacing(5),
    marginTop: theme.spacing(0.8),
    marginLeft: theme.spacing(0)
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: '-10px',
    width: theme.spacing(200),
  },
  predictgraph: {
    marginLeft: '-20px',
    marginTop: '30px'
  },
  databaseStyle: { color: "black" },
  center: {
    display: 'flex',
    justify_content: 'center',
    align_items: 'center',
    marginTop: '20%'
  },
  footertxt: {
    width: "134 px",
    height: "18 px",
    fontFamily: "Roboto",
    fontSize: "16px",
    fontWeight: "normal",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.39",
    letterSpacing: "normal",
    color: "#004162",
  },
  predictbutton: {
    // boxShadow: '1000',
    textTransform: 'none',
    fontSize: 16,
    color: 'white',
    padding: '6px 12px',
    border: '1px solid',
    lineHeight: 1.95,
    backgroundColor: '#c73232',
    borderColor: '#c73232',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#c73232',
      borderColor: '#c73232',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#c73232',
      borderColor: '#c73232',
    },
    '&:focus': {
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      outline: "none"
    },
  },
  backbutton: {
    // boxShadow: '1000',
    textTransform: 'none',
    fontSize: 16,
    color: 'white',
    padding: '4px 8px',
    border: '1px solid',
    lineHeight: 1.95,
    backgroundColor: '#c73232',
    borderColor: '#c73232',
    marginLeft:'2%',
    marginTop:'-5%',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#c73232',
      borderColor: '#c73232',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#c73232',
      borderColor: '#c73232',
    },
    '&:focus': {
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      outline: "none"
    },
  },
  buttonMargin: {
    marginLeft: theme.spacing(123)
  },
  
  margin: {
    margin: theme.spacing(1),
  },
  margin1: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(3),
  },
  margin2: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(5),
  },
  formControl: {
    marginLeft: theme.spacing(6),
    width: theme.spacing(21),
  },
  selectEmpty: {
    marginTop: theme.spacing(0.5),
  },
  CsvIcon: {
    marginTop: theme.spacing(2.5),
    paddingLeft: theme.spacing(2)
  },
  check: {
    paddingLeft: theme.spacing(2),
  },
  check2: {
    paddingLeft: theme.spacing(0),
  },
  check3: {
    paddingLeft: theme.spacing(2),
  },
  check4: {
    paddingLeft: theme.spacing(2)
  },
  root1: {
    '&:hover': {
      backgroundColor: 'transparent',
    }
  },
  label: {
    width: theme.spacing(28),
    color: 'Black'
  },
  label1: {
    color: '#403a3a',
    width: theme.spacing(10),
  },
  icon: {
    width: theme.spacing(2),
    height: theme.spacing(2),
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: 'none'
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#004162',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: theme.spacing(2),
      height: theme.spacing(2),
      backgroundImage:
        "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
        " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
        "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: '#004162',
    },
  },
  card: {
    marginLeft: theme.spacing(7),
    marginTop: theme.spacing(3)

  },
  typo: {
    font: "revert"
  },
  form: {
    position: "relative",
    top: "-30px",
    left: "15px",
  },
  csvlogo: {
    width: 60,
    marginTop: theme.spacing(1.5),
    marginLeft: theme.spacing(0)
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '50%',
    maxHeight: '50%'
  },
});

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase);

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 32,
    height: 20,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: '#52d869',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: '#52d869',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 18,
    height: 18,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});

const styles = {
  row: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: '-15px',
    width: theme.spacing(200),
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
    color: "black"
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
}

const useStylesBootstrap = makeStyles((theme) => ({
  arrow: {
    color: theme.palette.common.black,
  },
  tooltip: {
    backgroundColor: theme.palette.common.black,
  },
}));
// Inspired by blueprintjs

function BootstrapTooltip(props) {
  const classes = useStylesBootstrap();

  return <Tooltip arrow classes={classes} {...props} />;
}


class Predictcsv extends Component {

  // handleSelect(ranges){
  //   console.log(ranges);
  //   // {
  //   //   selection: {
  //   //     startDate: [native Date Object],
  //   //     endDate: [native Date Object],
  //   //   }
  //   // }
  // }

  constructor(props) {
    super(props);
    this.handleParameters = this.handleParameters.bind(this);
    // this.Model2Calls = this.Model2Calls.bind(this);
    // this.handleRangeSelect = this.handleRangeSelect.bind(this);
    // this.daysOfPrediction = this.handleRangeSelect.value.bind(this);
    this.state = {
      status: 3,
      workDays: [],
      statusActual: 1,
      toggleStatus: 'Off',
      parameters: '',
      model1: 1,
      model2: 3,
      model3: 5,
      model: '',
      DataScientist: 10,
      startDate: '',
      endDate: '',
      loading: false,
      loadingstatus: 1,
      // DataScientist: 10,
      CustomerStatus: this.props.Customer1,
      open: false, mysql: "",
      dateRange: "",
      daysOfPrediction: '',
      tableName: null,
      // dataPoints: [], dataPoints1: [], dataPoints2: [], dataPoints3: [],
      // dataPoints4: [], dataPoints5: [], dataPoints6: [], isLoaded: false,
      dataCheckBox: '',
      isLoadedActualModel: false,
      open: false,
      selectModel: false
    };
    console.log(this.state.loadingstatus);
    this.predict = this.predict.bind(this);
    this.handleChange = this.handleChange.bind(this);
    // 0: no show, 1: show yes, 2: show no.
  }

  handleChange({ target }) {
    this.setState({
      [target.name]: target.value
    });
  }

  predict() {
    console.log(this.state.tableName, this.state.daysOfPrediction);
  }

  radioHandler = (status) => {
    this.setState({ status });
  }

  radioHandlerActual = (statusActual) => {
    this.setState({ statusActual });
  }

  handleCheckboxChange = (event) => {
    let newArray = [...this.state.workDays, event.target.id];
    if (this.state.workDays.includes(event.target.id)) {
      newArray = newArray.filter((day) => day !== event.target.id);
    }
    this.setState({
      workDays: newArray
    });
  };

  handleStatusModel1Change() {
    if (this.state.model1 == 1) {
      this.setState({
        model1: 2
      })
    }
    else {
      this.setState({
        model1: 1
      })
    }
  };

  handleStatusModel2Change() {
    if (this.state.model2 == 3) {
      this.setState({
        model2: 4
      })
    }
    else {
      this.setState({
        model2: 3
      })
    }
  };

  handleStatusModel3Change() {
    if (this.state.model3 == 5) {
      this.setState({
        model3: 6
      })
    }
    else {
      this.setState({
        model3: 5
      })
    }
  };


  twoCalls = (event) => {
    this.handleCheckboxChange(event);
    this.handleStatusModel1Change();
  };

  Model2Calls = (event) => {

    this.handleCheckboxChange(event);
    this.handleStatusModel2Change();
    this.setState({ model: this.state.model2 });
  };

  Model3Calls = (event) => {
    this.handleCheckboxChange(event);
    this.handleStatusModel3Change();
  }

  handleToggle = () => {
    if (this.state.toggleStatus == 'Off')
      this.setState({
        toggleStatus: 'On'
      })
    else {
      this.setState({
        toggleStatus: 'Off'
      })
    }
  }

  handleParameters(event) {
    var name = event.target.name;
    this.setState({
      ...this.state,
      [name]: event.target.value,
    });
  }

  predictCsv = event => {
    this.setState({

      loadingstatus: 2
    })

  }

  backToSelection = event => {
    this.setState({

      loadingstatus: 1
    })

  }



  // predictCsv = event => {
  //   const predictData = {
  //     model: this.state.model2,
  //     startDate: this.state.startDate,
  //     endDate: this.state.endDate,
  //     daysOfPrediction: this.state.daysOfPrediction
  //   };
  //   this.setState({
  //     loading: true,
  //     loadingstatus: false

  //   })
  //   const requestOptions = {
  //     method: 'POST',
  //     headers: { 'Content-Type': 'application/json' },
  //     // body: JSON.stringify({ model: this.state.model2,
  //     //   startDate: this.state.startDate,
  //     //   endDate: this.state.endDate,
  //     //   daysOfPrediction: this.state.daysOfPrediction })
  //     body:JSON.stringify(predictData)
  // };

  //   //  axios.post("https://api.mocki.io/v1/a3d18a98", predictData)
  //   fetch("https://api.mocki.io/v1/3a38a700", requestOptions)
  //     .then(res => res.json())
  //     .then(
  //       (data) => {
  //         var dpt = [];
  //         var dpt1 = [];
  //         var dpt2 = [];
  //         var dpt3 = [];
  //         // var dpt4= [];
  //         // var dpt5= [];
  //         // var dpt6= [];

  //         for (var i = 0; i < data.length; i++) {

  //           if (data[i].reservationCountActual != "") {
  //             dpt.push({
  //               x: new Date(data[i].operatingDate),
  //               y: Number(data[i].reservationCountActual)
  //             });

  //           }
  //           if (data[i].reservationCountLstm != "") {
  //             dpt1.push({
  //               x: new Date(data[i].operatingDate),
  //               y: Number(data[i].reservationCountLstm)
  //             });
  //             // dpt4.push({
  //             //   x: new Date(data[i].operatingDate),
  //             //   y: Number(data[i].MAPE_error)
  //             // });
  //           }
  //           if (data[i].reservationCountCnn != "") {
  //             dpt2.push({
  //               x: new Date(data[i].operatingDate),
  //               y: Number(data[i].reservationCountCnn)
  //             });
  //             // dpt5.push({
  //             //   x: new Date(data[i].operatingDate),
  //             //   y: Number(data[i].MAPE_error)
  //             // });
  //           }
  //           if (data[i].reservationCountGru != "") {
  //             dpt3.push({
  //               x: new Date(data[i].operatingDate),
  //               y: Number(data[i].reservationCountGru)
  //             });
  //             // dpt6.push({
  //             //   x: new Date(data[i].operatingDate),
  //             //   y: Number(data[i].MAPE_error)
  //             // });
  //           }

  //           // alert(dpt.pop());
  //         }

  //         this.setState({
  //           loadingrate:100,
  //           isLoadedActualModel: true,
  //           dataPoints: dpt,
  //           dataPoints1: dpt1,
  //           dataPoints2: dpt2,
  //           dataPoints3: dpt3,  
  //           loading: false,
  //           loadingstatus: true,
  //           // dataPoints4: dpt4,
  //           // dataPoints5:dpt5,
  //           // dataPoints6:dpt6
  //         });
  //       }
  //     ).catch((error) => {
  //       console.log(error)
  //     });
  //   // console.log("Dates");
  //   // console.log(this.state.startDate, this.state.endDate);
  //   // console.log(this.state.model);
  //   // console.log(this.state.daysOfPrediction);
  //   console.log("dataPoints:" + JSON.stringify(this.state.dataPoints));
  //   console.log("dataPoints:" + JSON.stringify(this.state.dataPoints1));
  //   console.log("dataPoints:" + JSON.stringify(this.state.dataPoints2));
  //   console.log("dataPoints:" + JSON.stringify(this.state.dataPoints3));
  //   this.setState({ model: '', startDate: '', endDate: '', daysOfPrediction: '' });
  // }

  componentDidMount() {
    const selectionRange = {
      startDate: new Date(new Date().setDate(new Date().getDate() - 29)),
      endDate: new Date(),
      key: 'selection'
    };
    this.setState({
      dateRange: selectionRange,
      loading: false
    })

  }


  render() {
    let graph;
    const selectionRange = {
      startDate: new Date(),
      endDate: new Date(),
      key: 'selection'
    };

    const { classes, values, handleChange, onChangeCheckbox, validatorListener, disabled, handleReset } = this.props;
    // const { status,startDate, setStartDate, endDate, setEndDate  } = this.state;
    // const [age, setAge] = React.useState('');
    // const handleChange = (event) => {
    //   setAge(event.target.value);
    // };

    const handleSelect = (ranges) => {
      console.log(ranges);
      // {
      //   selection: {
      //     startDate: [native Date Object],
      //     endDate: [native Date Object],
      //   }
      selectionRange.startDate = ranges.selection.startDate;
      selectionRange.endDate = ranges.selection.endDate;
      selectionRange.key = ranges.selection.key;

      // }

    }
    const handleRangeSelect = (value) => {
      if (value !== null) {
        selectionRange.startDate = new Date(new Date().setDate(new Date().getDate() - (value - 1)));
        selectionRange.endDate = new Date();
        selectionRange.key = 'selection';
      }


      this.setState({
        dateRange: selectionRange,
        startDate: selectionRange.startDate, endDate: selectionRange.endDate,
        daysOfPrediction: value
      })


      // if (value === "40") {
      //   console.log(value);
      //   selectionRange.startDate = new Date(new Date().setDate(new Date().getDate() - 40));
      //   selectionRange.endDate = new Date();
      //   selectionRange.key = 'selection';
      //   this.setState({ startDate: selectionRange.startDate, endDate: selectionRange.endDate });
      //   this.setState({ daysOfPrediction: value })
      // }

      // else if (value === "30") {
      //   selectionRange.startDate = new Date(new Date().setDate(new Date().getDate() - 30));
      //   selectionRange.endDate = new Date();
      //   selectionRange.key = 'selection';
      //   this.setState({ startDate: selectionRange.startDate, endDate: selectionRange.endDate });
      //   this.setState({ daysOfPrediction: value })
      // }


      // }


    }


    // const options = {
    //   title: {
    //     text: ""
    //   },
    //   exportEnabled: true,
    //   theme: "light2",
    //   subtitles: [{
    //     text: ""
    //   }],
    //   charts: [{
    //     axisX: {
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "MMM DD YYYY"
    //       }
    //     },
    //     axisY: {
    //       title: "Stand By Count",
    //       suffix: "",
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "#"
    //       }
    //     },
    //     toolTip: {
    //       shared: true
    //     },
    //     data: [
    //       {
    //         name: "Actual Standby Count",
    //         type: "spline",
    //         showInLegend: true,
    //         legendText: "Actual Standby Count",
    //         color: "#1f78b4",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints
    //       },
    //     ],
    //   }],
    //   navigator: {
    //     slider: {
    //       minimum: new Date("01-02-2019"),
    //       maximum: new Date("01-12-2021")
    //     }
    //   }
    // };

    // const optionsActualModel1 = {
    //   title: {
    //     text: ""
    //   },
    //   exportEnabled: true,
    //   theme: "light2",
    //   subtitles: [{
    //     text: ""
    //   }],
    //   charts: [{
    //     axisX: {
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "MMM DD YYYY"
    //       }
    //     },
    //     axisY: {
    //       title: "Stand By Count",
    //       suffix: "",
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "#"
    //       }
    //     },
    //     toolTip: {
    //       shared: true
    //     },
    //     data: [
    //       {
    //         name: "Actual Standby Count",
    //         type: "spline",
    //         color: "#1f78b4",
    //         showInLegend: true,
    //         legendText: "Actual Standby Count",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints
    //       },
    //       {
    //         name: "LSTM Standby Count",
    //         type: "spline",
    //         color: "#f3b813",
    //         showInLegend: true,
    //         legendText: "LSTM",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints1
    //       },
    //     ],
    //   }],
    //   navigator: {
    //     slider: {
    //       minimum: new Date("01-02-2019"),
    //       maximum: new Date("01-12-2021")
    //     }
    //   }
    // };

    // const optionsActualModel1Model2 = {
    //   title: {
    //     text: ""
    //   },
    //   exportEnabled: true,
    //   theme: "light2",
    //   subtitles: [{
    //     text: ""
    //   }],
    //   charts: [{
    //     axisX: {
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "MMM DD YYYY"
    //       }
    //     },
    //     axisY: {
    //       title: "Stand By Count",
    //       suffix: "",
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "#"
    //       }
    //     },
    //     toolTip: {
    //       shared: true
    //     },
    //     data: [
    //       {
    //         name: "Actual Standby Count",
    //         type: "spline",
    //         color: "#1f78b4",
    //         showInLegend: true,
    //         legendText: "Actual Standby Count",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints
    //       },
    //       {
    //         name: "LSTM Predicted Count",
    //         type: "spline",
    //         color: "#f3b813",
    //         showInLegend: true,
    //         legendText: "LSTM",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints1
    //       },
    //       {
    //         name: "CNN Predicted Count",
    //         type: "spline",
    //         color: "#80271b",
    //         showInLegend: true,
    //         legendText: "CNN",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints2
    //       },
    //     ],
    //   }],
    //   navigator: {
    //     slider: {
    //       minimum: new Date("01-02-2019"),
    //       maximum: new Date("01-12-2021")
    //     }
    //   }
    // };

    // const optionsActualModel2 = {
    //   title: {
    //     text: ""
    //   },
    //   exportEnabled: true,
    //   theme: "light2",
    //   subtitles: [{
    //     text: ""
    //   }],
    //   charts: [{
    //     axisX: {
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "MMM DD YYYY"
    //       }
    //     },
    //     axisY: {
    //       title: "Stand By Count",
    //       suffix: "",
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "#"
    //       }
    //     },
    //     toolTip: {
    //       shared: true
    //     },
    //     data: [
    //       {
    //         name: "Actual Standby Count",
    //         type: "spline",
    //         color: "#1f78b4",
    //         showInLegend: true,
    //         legendText: "Actual Standby Count",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints
    //       },
    //       {
    //         name: "CNN Predicted Count",
    //         type: "spline",
    //         color: "#80271b",
    //         showInLegend: true,
    //         legendText: "CNN",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints2
    //       },
    //     ],
    //   }],
    //   navigator: {
    //     slider: {
    //       minimum: new Date("01-02-2019"),
    //       maximum: new Date("01-12-2021")
    //     }
    //   }
    // };

    // const optionsActualModel1Model2Model3 = {
    //   title: {
    //     text: ""
    //   },
    //   exportEnabled: true,
    //   theme: "light2",
    //   subtitles: [{
    //     text: ""
    //   }],
    //   charts: [{
    //     axisX: {
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "MMM DD YYYY"
    //       }
    //     },
    //     axisY: {
    //       title: "Stand By Count",
    //       suffix: "",
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "#"
    //       }
    //     },
    //     toolTip: {
    //       shared: true
    //     },
    //     data: [
    //       {
    //         name: "Actual Standby Count",
    //         type: "spline",
    //         color: "#1f78b4",
    //         showInLegend: true,
    //         legendText: "Actual Standby Count",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints
    //       },
    //       {
    //         name: "LSTM Predicted Count",
    //         type: "spline",
    //         color: "#f3b813",
    //         showInLegend: true,
    //         legendText: "LSTM",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints1
    //       },
    //       {
    //         name: "CNN Predicted Count",
    //         type: "spline",
    //         color: "#80271b",
    //         showInLegend: true,
    //         legendText: "CNN",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints2
    //       },
    //       {
    //         name: "GRU Predicted Count",
    //         type: "spline",
    //         color: "#28a647",
    //         showInLegend: true,
    //         legendText: "GRU",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints3
    //       },

    //     ],
    //   }],
    //   navigator: {
    //     slider: {
    //       minimum: new Date("01-02-2019"),
    //       maximum: new Date("01-12-2021")
    //     }
    //   }
    // };

    // const optionsActualModel1Model3 = {
    //   title: {
    //     text: ""
    //   },
    //   exportEnabled: true,
    //   theme: "light2",
    //   subtitles: [{
    //     text: ""
    //   }],
    //   charts: [{
    //     axisX: {
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "MMM DD YYYY"
    //       }
    //     },
    //     axisY: {
    //       title: "Stand By Count",
    //       suffix: "",
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "#"
    //       }
    //     },
    //     toolTip: {
    //       shared: true
    //     },
    //     data: [
    //       {
    //         name: "Actual Standby Count",
    //         type: "spline",
    //         color: "#1f78b4",
    //         showInLegend: true,
    //         legendText: "Actual Standby Count",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints
    //       },
    //       {
    //         name: "LSTM Predicted Count",
    //         type: "spline",
    //         color: "#f3b813",
    //         showInLegend: true,
    //         legendText: "LSTM",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints1
    //       },
    //       {
    //         name: "GRU Predicted Count",
    //         type: "spline",
    //         color: "#28a647",
    //         showInLegend: true,
    //         legendText: "GRU",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints3
    //       },

    //     ],
    //   }],
    //   navigator: {
    //     slider: {
    //       minimum: new Date("01-02-2019"),
    //       maximum: new Date("01-12-2021")
    //     }
    //   }
    // };

    // const optionsActualModel2Model3 = {
    //   title: {
    //     text: ""
    //   },
    //   exportEnabled: true,
    //   theme: "light2",
    //   subtitles: [{
    //     text: ""
    //   }],
    //   charts: [{
    //     axisX: {
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "MMM DD YYYY"
    //       }
    //     },
    //     axisY: {
    //       title: "Stand By Count",
    //       suffix: "",
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "#"
    //       }
    //     },
    //     toolTip: {
    //       shared: true
    //     },
    //     data: [
    //       {
    //         name: "Actual Standby Count",
    //         type: "spline",
    //         color: "#1f78b4",
    //         showInLegend: true,
    //         legendText: "Actual Standby Count",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints
    //       },
    //       {
    //         name: "CNN Predicted Count",
    //         type: "spline",
    //         color: "#80271b",
    //         showInLegend: true,
    //         legendText: "CNN",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints2
    //       },
    //       {
    //         name: "GRU Predicted Count",
    //         type: "spline",
    //         color: "#28a647",
    //         showInLegend: true,
    //         legendText: "GRU",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints3
    //       },

    //     ],
    //   }],
    //   navigator: {
    //     slider: {
    //       minimum: new Date("01-02-2019"),
    //       maximum: new Date("01-12-2021")
    //     }
    //   }
    // };

    // const optionsActualModel3 = {
    //   title: {
    //     text: ""
    //   },
    //   exportEnabled: true,
    //   theme: "light2",
    //   subtitles: [{
    //     text: ""
    //   }],
    //   charts: [{
    //     axisX: {
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "MMM DD YYYY"
    //       }
    //     },
    //     axisY: {
    //       title: "Stand By Count",
    //       suffix: "",
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "#"
    //       }
    //     },
    //     toolTip: {
    //       shared: true
    //     },
    //     data: [
    //       {
    //         name: "Actual Standby Count",
    //         type: "spline",
    //         color: "#1f78b4",
    //         showInLegend: true,
    //         legendText: "Actual Standby Count",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints
    //       },
    //       {
    //         name: "GRU Predicted Count",
    //         type: "spline",
    //         color: "#28a647",
    //         showInLegend: true,
    //         legendText: "GRU",
    //         yValueFormatString: "#",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints3
    //       },

    //     ],
    //   }],
    //   navigator: {
    //     slider: {
    //       minimum: new Date("01-02-2019"),
    //       maximum: new Date("01-12-2021")
    //     }
    //   }
    // };

    // const options_MAPE_Error_Actualodel1Model2Model3 = {
    //   title: {
    //     text: ""
    //   },
    //   exportEnabled: true,
    //   theme: "light2",
    //   subtitles: [{
    //     text: ""
    //   }],
    //   charts: [{
    //     axisX: {
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "MMM DD YYYY"
    //       }
    //     },
    //     axisY: {
    //       title: "MAPE Error",
    //       suffix: "%",
    //       crosshair: {
    //         enabled: true,
    //         snapToDataPoint: true,
    //         valueFormatString: "# '%'"
    //       }
    //     },
    //     toolTip: {
    //       shared: true
    //     },
    //     data: [
    //       {
    //         name: "LSTM MAPE Error",
    //         type: "spline",
    //         suffix: "%",
    //         color: "#f3b813",
    //         showInLegend: true,
    //         legendText: "LSTM",
    //         yValueFormatString: "# '%'",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints4
    //       },
    //       {
    //         name: "CNN MAPE Error",
    //         type: "spline",
    //         color: "#80271b",
    //         suffix: "%",
    //         showInLegend: true,
    //         legendText: "CNN",
    //         yValueFormatString: "# '%'",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints5
    //       },
    //       {
    //         name: "GRU MAPE Error",
    //         type: "spline",
    //         suffix: "%",
    //         color: "#28a647",
    //         showInLegend: true,
    //         legendText: "GRU",
    //         yValueFormatString: "# '%'",
    //         xValueFormatString: "MMM DD YYYY",
    //         dataPoints: this.state.dataPoints6
    //       },

    //     ],
    //   }],
    //   navigator: {
    //     slider: {
    //       minimum: new Date("01-02-2019"),
    //       maximum: new Date("01-12-2021")
    //     }
    //   }
    // };

    // const containerProps = {
    //   width: "100%",
    //   height: "450px",
    //   margin: "auto"
    // };

    // if ((this.props.workDays[0]=='Model1') && (this.props.workDays[1]==undefined) && (this.props.toggleStatus!=='On')){
    //   console.log('hello actual model1')
    //   graph = <CanvasJSStockChart containerProps={containerProps} options = {optionsActualModel1}/>;
    // }
    // else if ((((this.props.workDays[0]=='Model1') && (this.props.workDays[1]== 'Model2') && (this.props.toggleStatus!=='On')) ||
    //  ((this.props.workDays[0]=="Model2") && (this.props.workDays[1]=="Model1") && (this.props.toggleStatus!=='On')))
    //  &&(this.props.workDays[2]!=='Model3')){
    //   {console.log('hello combined')}
    //   graph = <CanvasJSStockChart containerProps={containerProps} options = {optionsActualModel1Model2}/>;
    // }
    // else if ((this.props.workDays[0]=='Model2') && (this.props.workDays[1]==undefined) && (this.props.toggleStatus!=='On')){
    //   {console.log('hello actual model 2')}
    //   graph = <CanvasJSStockChart containerProps={containerProps} options = {optionsActualModel2}/>;
    // }
    // if ((this.state.model3 != 'GRU') && (this.state.model2 == 'CNN') &&
    //   (this.state.model1 != 'LSTM') && (this.state.toggleStatus != 'On')) {
    //   console.log('hello actual model 3')
    //   graph = <CanvasJSStockChart containerProps={containerProps} options={optionsActualModel3} />;
    // }
    // else if ((this.state.model1 == 'LSTM') && (this.state.model2 == 'CNN') &&
    //   (this.state.model3 == 'GRU') && (this.state.toggleStatus != 'On') && (this.state.parameters != 'MAPE Error')) {
    //   console.log('hello actual model 1 2 3')
    //   graph = <CanvasJSStockChart containerProps={containerProps} options={optionsActualModel1Model2Model3} />;
    // }
    // else if ((this.state.model2 == 'CNN') && (this.state.model3 == 'GRU') && (this.state.parameters != 'MAPE Error') && (this.state.toggleStatus !== 'On')) {
    //   console.log('hello actual model 2 3')
    //   graph = <CanvasJSStockChart containerProps={containerProps} options={optionsActualModel2Model3} />;
    // }
    // else if (((this.state.model1 == 'LSTM') && (this.state.model3 == 'GRU') && (this.state.parameters != 'MAPE Error') && (this.state.toggleStatus !== 'On'))) {
    //   console.log('hello actual model 1 3')
    //   graph = <CanvasJSStockChart containerProps={containerProps} options={optionsActualModel1Model3} />;
    // }
    // else if ((this.state.parameters == 'MAPE Error') && (this.state.model1 == 'LSTM') && (this.state.model2 == 'CNN') &&
    //   (this.props.model3 == 'GRU') && (this.props.toggleStatus != 'On')) {
    //   console.log('hello inside MAPE Error')
    //   graph = <CanvasJSStockChart containerProps={containerProps} options={options_MAPE_Error_Actualodel1Model2Model3} />;
    // }
    // else if ((this.props.toggleStatus=='On')){
    //   console.log('inside kpi');
    //   graph = <KPI/>
    // }
    // else {
    //   { console.log('hello actual') }
    //   graph = <CanvasJSStockChart containerProps={containerProps} options={options} />;
    // }
    // var disable = {
    //   display: this.state.loading ? 'none' : 'block'
    // }



    return (
              <div>
                 {(this.state.loadingstatus == 1) ?
                 <div>        
                <h6 className={classes.row}><b>Forecasting Models</b></h6>
                <p className={classes.row}>Fill the details of the forecasting call to all the respective models as input starting from the date</p>
               
                <FormGroup >
                <div>
                <h6 className={classes.row}><b>Select Models</b></h6>
                {/* <FormControlLabel
              className={classes.label}
              control={<Checkbox
                className={classes.check}
                style={{
                  color: "grey",
                }}
                defaultChecked
                disabled
                id="1"
                value="Actual Model"
                onChange={this.handleCheckboxChange}
                onClick={(e) => this.radioHandlerActual(1)}
              />}
              label="Actual Standby Count"
            /> */}
                <FormControlLabel
                  className={classes.label1}
                  control={<Checkbox
                    className={classes.check2}
                    style={{
                      color: "#004162",
                    }}
                    id="Model1"
                    value="LSTM"
                    onChange={this.twoCalls}
                    onClick={(e) => this.radioHandler(1)}
                  />}
                  label="LSTM"
                />


                {(this.props.status == undefined) ?
                  <FormControlLabel
                    className={classes.label1}
                    control={<Checkbox
                      className={classes.check3}
                      style={{
                        color: "#004162",
                      }}
                      id="Model2"
                      value="CNN"
                      onChange={this.Model2Calls}
                      onClick={(e) => this.radioHandler(2)}
                    />}
                    label="CNN"
                  /> : null}

                {console.log(this.props.status)}
                {(this.props.status !== 1) ?
                  <FormControlLabel
                    className={classes.label1}
                    control={<Checkbox
                      className={classes.check4}
                      style={{
                        color: "#004162",
                      }}
                      id="Model3"
                      value="GRU"
                      onChange={this.Model3Calls}
                      onClick={(e) => this.radioHandler(3)}
                    />}
                    label="GRU"
                  /> : null}
            </div>

                <div style={styles.row}>
               
                  <FormControl className={classes.margin1}>
                    <TextField id="outlined-basic" label="Table Name"
                      name="tableName"
                      value={this.state.tableName}
                      onChange={this.handleChange}
                      placeholder="Enter table name"
                      variant="outlined" />
                  </FormControl>
                  <FormControl variant="outlined" className={classes.margin1}>
                    {/* <InputLabel >Table Names</InputLabel>
        <Select
          labelId="demo-customized-select-label"
          id="demo-customized-select"
          label="Age"
          className={classes.root}
          // value={age}
          // onChange={handleChange}
          input={<BootstrapInput />}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </Select> */}
                    <InputLabel >
                      Tables
                </InputLabel>
                    <Select
                      native
                      // onChange={handleChange("dbms")}
                      // onInput={handleChange("dbms")}
                      // value={values.dbms}
                      // margin="normal"
                      label="Database Type"
                      className={classes.root}
                      inputProps={{
                        name: classes.input,
                        id: "outlined-age-native-simple",
                      }}
                    >
                      {/* <option aria-label="None" value="" /> */}
                      <option value={"Crew Table"} className={classes.databaseStyle}>Crew Table</option>
                      <option value={"Client Table"} className={classes.databaseStyle}>Client Table</option>
                    </Select>
                  </FormControl>
                  <FormControl variant="outlined" className={classes.margin1}>
                    <InputLabel >
                      Days
                </InputLabel>
                    <Select
                      native
                      // onChange={handleChange("dbms")}
                      // onInput={handleChange("dbms")}
                      onChange={e => { handleRangeSelect(e.target.value); }}
                      // value={values.dbms}
                      // margin="normal"
                      label="Database Type"
                      className={classes.root}
                      inputProps={{
                        name: classes.input,
                        id: "outlined-age-native-simple",
                        defaultValue: 30
                      }}
                    >
                      {/* <option aria-label="None" value="" /> */}
                      <option value={15} className={classes.databaseStyle}>Last 15 days</option>
                      <option value={20} className={classes.databaseStyle}>Last 20 days</option>
                      <option value={25} className={classes.databaseStyle}>Last 25 days</option>
                      <option value={30} className={classes.databaseStyle}>Last 30 days</option>
                      <option value={35} className={classes.databaseStyle}>Last 35 days</option>
                      <option value={40} className={classes.databaseStyle}>Last 40 days</option>
                      <option value={45} className={classes.databaseStyle}>Last 45 days</option>
                      <option value={50} className={classes.databaseStyle}>Last 50 days</option>
                      <option value={55} className={classes.databaseStyle}>Last 55 days</option>
                      <option value={60} className={classes.databaseStyle}>Last 60 days</option>
                      <option value={65} className={classes.databaseStyle}>Last 65 days</option>
                      <option value={70} className={classes.databaseStyle}>Last 70 days</option>
                      <option value={75} className={classes.databaseStyle}>Last 75 days</option>
                      <option value={80} className={classes.databaseStyle}>Last 80 days</option>
                      <option value={85} className={classes.databaseStyle}>Last 85 days</option>
                      <option value={90} className={classes.databaseStyle}>Last 90 days</option>

                    </Select>
                  </FormControl>
                  <FormControl variant="outlined" className={classes.margin2}>


                  </FormControl>

                  <DateRange
                    style={{ pointerEvents: "none" }}
                    ranges={[this.state.dateRange]}
                    onChange={handleSelect}
                    className={classes.margin1}
                  />

                  {/* <DateRange
                ranges={[selectionRange]}
                onChange={handleSelect}
                className={classes.margin1}
              /> */}
                </div>
                {/* <DateRange
          editableDateInputs={true}
          onChange={item => setState([item.selection])}
          moveRangeOnFirstSelection={false}
          ranges={state}
          />
    
     */}



         

            <div>
                <FormControl className={classes.buttonMargin}>
                  <Button
                    variant="contained"
                    //   onClick={handleReset}
                    onClick={this.predictCsv}
                    className={classes.predictbutton}

                  >Forecast</Button>
                </FormControl>
              </div>

            </FormGroup>   </div>
         :
          // {(this.state.loadingstatus) ?
          <div className={classes.predictgraph}  >
            {/* <PredictionGraph {...this.state} /> */}
            {/* {graph} */}

   {/* <div className={classes.backButtonMargin}>
              <Button
                variant="contained"
                //   onClick={handleReset}
                onClick={this.backToSelection}
                className={classes.backbutton}

              >Back</Button>
            </div> */}

                           <div style={{marginTop:'-2%',paddingBottom:'2%'}}>
                  <Link onClick={this.backToSelection}>
                    <span className={classes.footertxt} style={{ marginLeft: '3%'}}> <ArrowBackIosIcon />Back</span>
                  </Link>
                </div>
                <div style={{marginLeft:'4%'}}>
                <h6 className={classes.row}><b>FORECASTING MODELS</b></h6>
                <p className={classes.row}>Fill the details of the forecasting call to all the respective models as input starting from the date</p>
                </div>
            <PredictionGraph {...this.state} />

         

          </div>

        }

      </div>
    );




  }
}

export default withStyles(useStyles)(Predictcsv)