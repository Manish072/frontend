/*!
=========================================================
* Material Dashboard React - v1.9.0
=========================================================
* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)
* Coded by Creative Tim
=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect, BrowserRouter } from "react-router-dom";
import LoginComponent from '../src/components/LoginComponent';
// import 'bootstrap/dist/css/bootstrap.min.css';
// core components
import Admin from "layouts/Admin.js";
import Customer from "layouts/Customer.js";
import DataEng from "layouts/DataEng.js";
import RTL from "layouts/RTL.js";
import "assets/css/material-dashboard-react.css?v=1.9.0";
import UserForm from "../src/components/Database_Configuration/UserForm";
//redux import
import { createStore } from 'redux';
import { valueState } from './reducer/valueState';
import {Provider} from 'react-redux';
import CustomerComponent from '../src/components/CustComponent/CustomerComponent';
import DataEngineerComponent from '../src/components/DataEngineer/DataEngineerComponent';
import CustomerLoginComp from '../src/components/CustomerLogin/CustomerLoginComp';
import PredictCsv from '../src/views/Predict/Predictcsv';

const hist = createBrowserHistory();
const store = createStore(valueState,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
  <Provider store={store}>
  <Router history={hist} >
    <BrowserRouter>
      <Switch>
        <Route exact path="/"><Redirect to="/login" /></Route> 
        <Route path="/login" exact component={LoginComponent} />
        <Route path="/customerlogin" exact component={CustomerLoginComp} />
        <Route path="/admin" component={Admin} />
        <Route path="/deploy" component={DataEng}/>
        <Route path="/custom" component={Customer} />
        {/* <Route path="/admin/table/" component={Admin} /> */}
        <Route path="/admin/table/database" exact component={UserForm} />
        <Route path="/predict" exact component={PredictCsv} />
        <Route path="/simulation" exact component={CustomerComponent}/>
        <Route path="/dataEng" exact component={DataEngineerComponent}/>
      </Switch>
    </BrowserRouter>
  </Router>
  </Provider>,
  document.getElementById("root")
);