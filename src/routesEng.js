import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LocationOn from "@material-ui/icons/LocationOn";
//import Notifications from "@material-ui/icons/Notifications";
import SettingsIcon from '@material-ui/icons/Settings';
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
import AssessmentIcon from '@material-ui/icons/Assessment';
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import UserProfile from "views/UserProfile/UserProfile.js";
//  import TableList from "views/TableList/TableList.js";
import NotificationsPage from "views/Notifications/Notifications.js";
import UserForm from "components/Database_Configuration/UserForm";
import CustomerComponent from "components/CustComponent/CustomerComponent.js";


const dashboardRoutes1 = [
  // {
  //   path: "/dashboard",
  //   name: "Dashboard",
  //   icon: Dashboard,
  //   component: DashboardPage,
  //   layout: "/custom"
  // },
  // {
  //   path: "/user",
  //   name: "Admin",
  //   icon: Person,
  //   component: UserProfile,
  //   layout: "/custom"
  // },
  {
    path: "/simulation",
    name: "Analytics",
    icon: AssessmentIcon,
    component: CustomerComponent,
    layout: "/custom"
  },
  {
    path: "/notifications",
    name: "Settings",
    icon: SettingsIcon,
    component: NotificationsPage,
    layout: "/custom"
  }
];

export default dashboardRoutes1;