/*!
=========================================================
* Material Dashboard React - v1.9.0
=========================================================
* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)
* Coded by Creative Tim
=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
//import Notifications from "@material-ui/icons/Notifications";
import SettingsIcon from '@material-ui/icons/Settings';
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
import AssessmentIcon from '@material-ui/icons/Assessment';
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import UserProfile from "views/UserProfile/UserProfile.js";
 import TableList from "views/TableList/TableList.js";
import NotificationsPage from "views/Notifications/Notifications.js";
import UserForm from "components/Database_Configuration/UserForm";
import TimelineIcon from '@material-ui/icons/Timeline';
import Predictcsv from 'views/Predict/Predictcsv.js';


const dashboardRoutes = [
  // {
  //   path: "/dashboard",
  //   name: "Dashboard",
  //   icon: Dashboard,
  //   component: DashboardPage,
  //   layout: "/admin"
  // },
  // {
  //   path: "/user",
  //   name: "Admin",
  //   icon: Person,
  //   component: UserProfile,
  //   layout: "/admin"
  // },
  {
    path: "/table/:tabId",
    name: "Analytics",
    icon: AssessmentIcon,
    component: TableList,
    layout: "/admin"
  },
  {
    path: "/notifications",
    name: "Settings",
    icon: SettingsIcon,
    component: NotificationsPage,
    layout: "/admin"
  },
  {
    path: "/predictcsv",
    name: "Forecast",
    icon: TimelineIcon,
    component: Predictcsv,
    layout: "/admin"
  }
];

export default dashboardRoutes;