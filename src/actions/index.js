export const simulation = () => {
    return {
        type: "SIMULATION"
    };
};
export const deployment = () => {
    return {
        type: "DEPOLYMENT"
    };
};
export const dbConfig = () => {
    return {
        type: "DBCONFIG"
    };
};

export const leftNavOpen = (open) => {
    return {
        type: "LEFTNAV",
        payload: open
    }
}