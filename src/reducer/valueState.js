const initialState = {
  openTab: 0,
  leftNavOpen: true
}

export const valueState = (state = initialState, action) => {

  switch (action.type) {
    case "SIMULATION":
      return {
        ...state,
        openTab: 1
      }
    case "DEPOLYMENT":
      return {
        ...state,
        openTab: 2
      }
    case "LEFTNAV":
        return {
          ...state,
          leftNavOpen : action.payload
        }
    default:
      return {
        ...state,
        openTab: 0
      }
  }
}