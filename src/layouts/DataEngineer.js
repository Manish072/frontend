import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import DataNavbar from "components/Navbars/DataNavbar.js";
import Footer from "components/Footer/Footer.js";
import DataSidebar from "components/Sidebar/DataSidebar.js";


import routesEng from "routesEng.js";

import styles from "assets/jss/material-dashboard-react/layouts/adminStyle.js";
import minstyles from "assets/jss/material-dashboard-react/layouts/minAdminStyle.js";

import bgImage from "assets/img/sidebar-2.jpg";
import logo from "assets/img/Emblem1.png";
import iblogo from "assets/img/ibslogo_revert.png";
import { useSelector } from 'react-redux';

let ps;

const switchRoutes = (
  <Switch>
    {routesEng.map((prop, key) => {
      if (prop.layout === "/dataeng") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      }
      return null;
    })}
    <Redirect from="/dataeng" to="/dataeng/table/dashboard" />
  </Switch>
);

// const useStyles = makeStyles(styles);

export default function Admin({ ...rest }) {
  const isNavOpen = useSelector(state => state.leftNavOpen);
  const useStyles = isNavOpen? makeStyles(styles) : makeStyles(minstyles);

  // styles
  const classes = useStyles();
  // ref to help us initialize PerfectScrollbar on windows devices
  const mainPanel = React.createRef();
  // states and functions
  const [image, setImage] = React.useState(bgImage);
  const [color, setColor] = React.useState("blue");
  const [fixedClasses, setFixedClasses] = React.useState("dropdown show");
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const handleImageClick = image => {
    setImage(image);
  };
  const handleColorClick = color => {
    setColor(color);
  };
  const handleFixedClick = () => {
    if (fixedClasses === "dropdown") {
      setFixedClasses("dropdown show");
    } else {
      setFixedClasses("dropdown");
    }
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const getRoute = () => {
    return window.location.pathname !== "/admin/maps";
  };
  const resizeFunction = () => {
    if (window.innerWidth >= 960) {
      setMobileOpen(false);
    }
  };
  // initialize and destroy the PerfectScrollbar plugin
  React.useEffect(() => {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(mainPanel.current, {
        suppressScrollX: true,
        suppressScrollY: false
      });
      document.body.style.overflow = "hidden";
    }
    window.addEventListener("resize", resizeFunction);
    // Specify how to clean up after this effect:
    return function cleanup() {
      if (navigator.platform.indexOf("Win") > -1) {
        ps.destroy();
      }
      window.removeEventListener("resize", resizeFunction);
    };
  }, [mainPanel]);
  return (
    <div className={classes.wrapper}>
      <DataSidebar
        routesEng={routesEng}
        logoText={"Crew Analytics"}
        logo={logo}
        iblogo={iblogo}
        ibText={"© 2020 ibssoftware. All Rights Reserved."}
        image={image}
        handleDrawerToggle={handleDrawerToggle}
        open={mobileOpen}
        color={color}
        {...rest}
      />
      <div className={classes.mainPanel} ref={mainPanel}>
        <DataNavbar
          routesEng={routesEng}
          handleDrawerToggle={handleDrawerToggle}
          {...rest}
        />
        {/* On the /maps route we want the map to be on full screen - this is not possible if the content and conatiner classes are present because they have some paddings which would make the map smaller */}
        {getRoute() ? (
          <div className={classes.content}>
            <div className={classes.container}>{switchRoutes}</div>
          </div>
        ) : (
          <div className={classes.map}>{switchRoutes}</div>
        )}
        {/* {getRoute() ?<Footer /> : null} */}
        
      </div>
    </div>
  );
}
